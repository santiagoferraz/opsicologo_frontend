<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PortfolioController@index')->name('index');
Route::get('/politica-de-privacidade', 'PagesController@pagePoliticaPrivacidade')->name('politica-de-privacidade');
Route::get('/quem-somos', 'PagesController@pageQuemSomos')->name('quem-somos');
Route::get('/termos-de-uso', 'PagesController@pageTermoUso')->name('termos-de-uso');
Route::get('/perguntas-frequentes', 'PagesController@pagePerguntasFrequentes')->name('perguntas-frequentes');
Route::get('/{link_psicologo}', 'HomePageController@index')->name('index');


$this->group(['prefix' => 'psicologo'], function () {
    Route::get('/consultas', 'ConsultasController@getConsultas')->name('consultas');
    Route::get('/get-horarios-disponiveis/{data}/{linkPsicologo}', 'ConsultasController@getHorariosDisponiveis')->name('horarios-disponiveis');
    Route::get('/valor', 'HomePageController@valor')->name('valor');
    Route::get('/lista-de-psicologos', 'PsicologosController@getListaPsicologosView')->name('lista-de-psicologos');
    Route::post('/inscricao-lead-lovers', 'HomePageController@inscricaoLeadLovers')->name('inscricao-lead-lovers');
    Route::post('/get-lista-de-psicologos/{pagina}', 'PsicologosController@getListaPsicologos')->name('get-lista-de-psicologos');
    Route::get('/ufs', 'PsicologosController@getUfs')->name('listar-ufs');
    Route::get('/destaques', 'PsicologosController@getPsicologosDestaque')->name('destaques');
    Route::get('/cidades/{idEstado}', 'PsicologosController@getCidades')->name('cidades');
    Route::post('/finalizar-agendamento', 'ConsultasController@criarAgendamento')->name('criar-agendamento');
    Route::post('/fale-conosco', 'HomePageController@enviarFaleConosco')->name('fale-conosco');


});