<div id="header-agendamento">
    <h3>Agende sua Sessão em apenas 4 passos</h3>
    <p>Abaixo você pode encontrar uma lista dos horários disponíveis para Atendimento Psicológico com o(a) Psicólogo(a)
        <b>{{ $psicologo->nome }}</b>. Escolha o melhor dia, horário e clique em AVANÇAR para prosseguir com o agendamento de
        sua sessão.</p>
</div>

<div id="modalSelecionarHorario" style="overflow: visible">
    <div id="conteudoSelecionarHorario">
    </div>
    <a href="#" rel="modal:close"></a>
</div>

<form id="dadosusuario" action="" style="display: none" name="dados-usuario">
    <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text" class="form-control required" name="nome" id="nome-user" placeholder="Informe seu nome">
    </div>
    <div class="form-group">
        <label for="email">E-mail</label>
        <input type="text" class="form-control required" name="email" id="email-user" placeholder="Informe seu e-mail">
    </div>
</form>
<div class="content" id="div-wizard">
    <div id="wizard">
        <h2>Horários</h2>
        <section>
            <div class="row">
                <div id="agendamento-perfil" class="col-lg-3 col-md-12 col-sm-12 col-xs-12 wow fadeInLeft">
                    <img id="agendamento-perfil-foto" src="{!! e(url($psicologo->fotoPerfil)) !!}">
                    <h4>{{ $psicologo->nome }}</h4>
                    <p>CRP-{{ $psicologo->crp }}</p>
                    <div id="agendamento-perfil-valor"><b>R$ </b>{{ $psicologo->consulta->valor }}</div>
                    <div id="agendamento-perfil-tempo"><img src="{{ asset('assets/img/icon_time_branco.png') }}"> 50 min</div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 wow fadeInDown" data-wow-delay="0.3s">
                    <center><div id="datetimepicker12"></div></center>
                </div>
                <div id="div-horarios" class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <center>
                        <div id="horarios-disponiveis" class="row"></div>
                    </center>
                    <div id="carregando-horarios" style="text-align: center">
                        <img style="margin-top: 80px" src="{{ asset('assets/img/carregando-circle.svg') }}" alt="">
                    </div>
                </div>
            </div>
        </section>


        <h2>Seus Dados</h2>
        <section>
            <center>
                <div class="row div-nome-email-agendamento">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="dadosuser"></div>
                </div>
            </center>
        </section>


        <h2>Confirmação</h2>
        <section>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h5 id="resumo-confirmacao">
                    </h5>
                    <div id="carregando-finalizar" style="text-align: center">
                        <img style="margin-top: 80px" src="{{ asset('assets/img/carregando-circle.svg') }}" alt="">
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="wizard" id="finalizacao-pagamento" >
    <div class="content">
        <div class="body" id="div-texto-pagamento">
            <img src="{{ asset('assets/img/check-circle.png') }}"><br>
        </div>
        <div class="body" id="div-texto-pagamento-2">
            <img src="{{ asset('assets/img/x-circle.png') }}"><br><b>Falha ao tentar agendar a consulta!</b><br><br>Não foi possível realizar o agendamento de sua consulta, por favor entre em contato com o psicólogo ou administrador desta página.<br><br>Pedimos desculpas pelo transtorno!
        </div>
    </div>
</div>



