<li class="nav-item">
    <a class="nav-link page-scroll" href="#home">Início</a>
</li>
<li class="nav-item">
    <a class="nav-link page-scroll" href="#app-features">Vantagens</a>
</li>
<li class="nav-item">
    <a class="nav-link page-scroll" href="#blog">Ver Psicólogos</a>
</li>
<li class="nav-item">
    <a class="nav-link page-scroll" href="https://pages.opsicologoonline.com.br/plataforma-opsicologoonline" target="_blank">PSI Trabalhe Conosco</a>
</li>
<li class="nav-item">
    <a class="nav-link page-scroll" href="#contact">Contato</a>
</li>
<li class="nav-item btn-cadastro-login">
    <a href="https://atendimento.opsicologoonline.com.br/sistema/cadastro.php" class="btn btn-lg btn-border btn-login">Cadastro</a>
</li>
<li class="nav-item btn-cadastro-login">
    <a href="https://atendimento.opsicologoonline.com.br/sistema/login.php" class="btn btn-lg btn-border btn-login">Login</a>
</li>