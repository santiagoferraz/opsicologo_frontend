<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Grayrids">

    <meta name="theme-color" content="#1C7BFF">
    <meta name="apple-mobile-web-app-status-bar-style" content="#1C7BFF">
    <meta name="msapplication-navbutton-color" content="#1C7BFF">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>O Psicologo Online</title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/template-principal/img/favicon_opsic.png') }}"/>
    <link rel="shortcut icon" type="image/ico" href="{{ asset('assets/template-principal/img/favicon_opsic.ico') }}"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/template-principal/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/template-principal/css/line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/template-principal/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/template-principal/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/template-principal/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/template-principal/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/template-principal/css/nivo-lightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/template-principal/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/template-principal/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/template-principal/css/portfolio.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/template-principal/css/psic-online.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/template-principal/css/font-awesome.min.css') }}">

    @yield('css')

</head>

<body>

<!-- Header Section Start -->
<header id="home" class="hero-area-2">
    <div class="overlay"></div>
    <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
        <div class="container">
            <a href="{{ URL::to('/') }}" class="navbar-brand"><img src="{{ asset('assets/template-principal/img/logo.png') }}" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="lni-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto w-100 justify-content-end">

                    @yield('menu-principal')

                </ul>
            </div>
        </div>
    </nav>

    @yield('carousel-area')

</header>
<!-- Header Section End -->


@yield('content')


<!-- Footer Section Start -->
<footer>
    <!-- Footer Area Start -->
    <section class="footer-Content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                    <center><img id="logo_rodape" src="{{ asset('assets/template-principal/img/logo_v.png') }}" alt="logotipo do opsicologo online"></center>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                    <h3 class="block-title">Certificado</h3>
                    <form name="pesquisar" action="https://cadastrosite.cfp.org.br/cadastro/siteAprovado.cfm" method="post" target="_blank">
                        <input type="hidden" id="nm_site" name="nm_site" size="50" value="">
                        <input type="hidden" id="url_site" name="url_site" size="50" value="atendimento.opsicologoonline.com.br">
                        <input type="hidden" id="solicitante" name="solicitante" size="50" value="">
                        <input type="hidden" id="protocolo" name="protocolo" size="50" value="">
                        <input type="hidden" id="comboEntidade" name="comboEntidade" size="50" value="">
                    </form>
                    <img src="{{ asset('assets/img/security.png') }}" id="img-security" alt="Dados Cryptografados Com Total Segurança Para o Usuário">
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                    <div class="widget">
                        <h3 class="block-title">Empresa</h3>
                        <ul class="menu">
                            <li><a href="{{ URL::to('/quem-somos') }}" target="_blank">Quem Somos</a></li>
                            <li><a href="{{ URL::to('/perguntas-frequentes') }}" target="_blank">Perguntas Frequentes</a></li>
                            <li><a href="{{ URL::to('/politica-de-privacidade') }}" target="_blank">Politica de Privacidade</a></li>
                            <li><a href="{{ URL::to('/termos-de-uso') }}" target="_blank">Termos de Uso</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                    <div class="widget">
                        <h3 class="block-title">Cadastro</h3>
                        <ul class="menu">
                            <li><a href="https://atendimento.opsicologoonline.com.br/sistema/cadastro.php" target="_blank">Sou Psicólogo</a></li>
                            <li><a href="https://atendimento.opsicologoonline.com.br/sistema/cadastro.php" target="_blank">Sou Paciente</a></li>
                            <li><a href="https://pages.opsicologoonline.com.br/plataforma-opsicologoonline" target="_blank">PSI Trabalhe Conosco</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
                    <div class="widget">
                        <h3 class="block-title">Suporte</h3>
                        <ul class="menu">
                            <li id="email-de-suporte">suporte@opsicologoonline.com.br</li>
                            <a href="https://api.whatsapp.com/send?phone=5519991449533&text=Tenho%20uma%20d%C3%BAvida%20sobre%20o%20Atendimento%20Online" target="_blank">
                                <div id="li-suporte">
                                    <img src="{{ asset('assets/template-principal/img/whatsapp-branco.png') }}" alt="Suporte para psicólogos via whatsapp">
                                    <span class="li-suporte-span1">Suporte para Psicólogos</span><br>
                                    <span class="li-suporte-span2">Horário de atendimento das 10h às 18h, exceto sábados, domingos e feriados. Responderemos nos dias e horários estabelecidos o mais breve possível</span>
                                </div>
                            </a>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Copyright Start  -->
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="site-info float-left">
                            <p>Copyright &copy; 2018 O Psicólogo Online - Todos os direitos reservados - Desenvolvido por
                                <a href="https://www.facebook.com/victorsetubal" target="_blank">Víctor Setúbal</a> e
                                <a href="https://www.linkedin.com/in/auberlansantiago" target="_blank">Santiago Ferraz</a></p>
                        </div>
                        <div class="float-right">
                            <ul class="footer-social">
                                <li><a class="facebook" href="https://www.facebook.com/opsicologoonline" title="Facebook" target="_blank"><i class="lni-facebook-filled"></i></a></li>
                                <li><a class="facebook" href="https://twitter.com/opsicologonline" title="Twitter" target="_blank"><i class="lni-twitter-filled"></i></a></li>
                                <li><a class="facebook" href="https://www.instagram.com/opsicologoonline/" title="Instagram" target="_blank"><i class="lni-instagram-filled"></i></a></li>
                                <li><a class="facebook" href="https://plus.google.com/+OpsicologoonlineBrpsi" title="Google+" target="_blank"><i class="lni-google-plus"></i></a></li>
                                <li><a class="facebook" href="https://www.youtube.com/c/OpsicologoonlineBrpsi" title="Youtube" target="_blank"><i class="lni-film-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Copyright End -->
    </section>
    <!-- Footer area End -->

</footer>
<!-- Footer Section End -->

<!-- Go To Top Link -->
<a href="#" class="back-to-top">
    <i class="lni-chevron-up"></i>
</a>

<!-- Preloader -->
<div id="preloader">
    <div class="loader" id="loader-1"></div>
</div>
<!-- End Preloader -->

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="{{ asset('assets/template-principal/js/jquery-min.js') }}"></script>
<script src="{{ asset('assets/template-principal/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/template-principal/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/template-principal/js/owl.carousel.js') }}"></script>
<script src="{{ asset('assets/template-principal/js/jquery.mixitup.js') }}"></script>
<script src="{{ asset('assets/template-principal/js/jquery.nav.js') }}"></script>
<script src="{{ asset('assets/template-principal/js/scrolling-nav.js') }}"></script>
<script src="{{ asset('assets/template-principal/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('assets/template-principal/js/wow.js') }}"></script>
<script src="{{ asset('assets/template-principal/js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('assets/template-principal/js/nivo-lightbox.js') }}"></script>
<script src="{{ asset('assets/template-principal/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/template-principal/js/waypoints.min.js') }}"></script>
{{--<script src="{{ asset('assets/template-principal/js/form-validator.min.js') }}"></script>--}}
{{--<script src="{{ asset('assets/template-principal/js/contact-form-script.js') }}"></script>--}}
<script src="{{ asset('assets/template-principal/js/main.js') }}"></script>

@yield('js')

</body>
</html>