<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="theme-color" content="#1C7BFF">
  <meta name="apple-mobile-web-app-status-bar-style" content="#1C7BFF">
  <meta name="msapplication-navbutton-color" content="#1C7BFF">

  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>O Psicólogo Online</title>

  <!-- Favicon -->
  <link rel="shortcut icon" type="image/png" href="{{ asset('assets/template-principal/img/favicon_opsic.png') }}"/>
  <link rel="shortcut icon" type="image/ico" href="{{ asset('assets/template-principal/img/favicon_opsic.ico') }}"/>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" >
  <!-- Icon -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/line-icons.css') }}">
  <!-- Slicknav -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slicknav.css') }}">
  <!-- Owl carousel -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.carousel.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.theme.css') }}">
  <!-- Slick Slider -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slick.css') }}" >
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slick-theme.css') }}" >
  <!-- Animate -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}">
  <!-- Main Style -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main.css') }}">
  <!-- Responsive Style -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.css') }}">
  <!-- css opsicologoonline -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/opsicologoonline.css') }}">
  <!-- css jquery steps -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/steps_css/jquery.steps.css') }}" >
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.modal.min.css') }}" >
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/steps_css/main.css') }}" >
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/datepicker/bootstrap-datetimepicker.css') }}" >


</head>
<body>

<!-- Header Area wrapper Starts -->
<header id="header-wrap">
  <!-- Navbar Start -->
  <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          <span class="icon-menu"></span>
          <span class="icon-menu"></span>
          <span class="icon-menu"></span>
        </button>
        <a href="{{ URL::to('/') }}" class="navbar-brand"><img src="{{ asset('assets/img/logo.png') }}"></a>
      </div>
    </div>

    <!-- Mobile Menu Start -->
    <ul class="mobile-menu navbar-nav">
      <li>
        <a class="page-scroll" href="#services">
          Início
        </a>
      </li>
      <li>
        <a class="page-scroll" href="#agendar-sessao">
          Agendar Sessão
        </a>
      </li>
      <li>
        <a class="page-scroll" href="#formacao-e-contato">
          Formação e Contato
        </a>
      </li>
    </ul>
    <!-- Mobile Menu End -->

  </nav>
  <!-- Navbar End -->

</header>
<!-- Header Area wrapper End -->

<!-- Services Section Start -->
<section id="services" class="section-padding">
  <div class="container">
      <div id="cabecalho-psicologo" class="container">
          <div class="row cabecalho-psicologo">
              <div id="div-foto-perfil-cabecalho" class="col-lg-2 col-md-12 col-sm-12 col-xs-12 wow fadeInLeft">
                  <img src="{!! e(url($psicologo->fotoPerfil)) !!}" id="foto-perfil-cabecalho">
              </div>
              <div id="dados-psicologo-cabecalho" class="col-lg-7 col-md-7 col-sm-12 col-xs-12 wow fadeInDown" data-wow-delay="0.3s">
                <div>
                  <h3>{{ $psicologo->nome }}</h3>
                  <div id="img">
                    <p>CRP-{{ $psicologo->crp }}</p>
                    <div id="valor"><b>R$ </b>{{ $psicologo->consulta->valor }}
                        <img src="{{ asset('assets/img/icon_time.png') }}"> 50 min
                    </div>
                  </div>
                </div>
              </div>
              <div id="btn-cabecalho" class="col-lg-3 col-md-5 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0.6s">
                  <a href="#agendar-sessao" class="btn btn-common btn-cabecalho">Agendar Sessão</a>
                  <a href="#formacao-e-contato" class="btn btn-common btn-cabecalho">Falar com o Psicólogo</a>
              </div>
              <div id="descricao-cabecalho" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.9s">
                  {!! $psicologo->descricao !!}
              </div>
          </div>
      </div>
  </div>
</section>
<!-- Services Section End -->


<!-- Agendar Sessão Section Start -->
<div id="agendar-sessao">
  <div class="container-fluid">

      @include('consultas')

  </div>
</div>
<!-- Agendar Sessão Section End -->

<!-- Formacao e Contato Section Start -->
<section id="formacao-e-contato" class="section-padding text-center">
    <div class="container">
        <div id="cabecalho-psicologo" class="container">
            <div class="row">
                <div id="div-formacao" class="col-lg-8 col-md-12 col-sm-12 col-xs-12 wow fadeInLeft">
                    <img src="{{ asset('assets/img/icon-graduation.png') }}"> Formação
                    <div id="text-formacao">
                        {!! $psicologo->formacao !!}
                    </div>
                </div>

                <div id="div-contato" class="col-lg-4 col-md-12 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0.6s">
                    <h2>Contato com o Psicólogo</h2>
                    <input style="display: none" id="email-psicologo" value="{{ $psicologo->email }}"/>
                    <div>
                        <img src="{{ asset('assets/img/icon-check.png') }}"> {{ $psicologo->email }}
                        <br>
                        <img src="{{ asset('assets/img/icon-check.png') }}"> {{ $psicologo->telefone }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Formacao e Contato Section End -->

<!-- Footer Section Start -->
<footer id="footer" class="footer-area section-padding">
  <div class="container">
    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
            <center>
                <form name="pesquisar" action="https://cadastrosite.cfp.org.br/cadastro/siteAprovado.cfm" method="post" target="_blank">
                    <input type="hidden" id="nm_site" name="nm_site" size="50" value="">
                    <input type="hidden" id="url_site" name="url_site" size="50" value="atendimento.opsicologoonline.com.br">
                    <input type="hidden" id="solicitante" name="solicitante" size="50" value="">
                    <input type="hidden" id="protocolo" name="protocolo" size="50" value="">
                    <input type="hidden" id="comboEntidade" name="comboEntidade" size="50" value="">
                </form>
                <img src="{{ asset('assets/img/security.png') }}" id="img-security" alt="Dados Cryptografados Com Total Segurança Para o Usuário">
            </center>
        </div>
        <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
          <div class="row">
              <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                  <div class="footer-logo mb-3">
                      <img src="{{ asset('assets/img/logo.png') }}" alt="">
                  </div>
              </div>
              <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.8s">
                  <div class="social-icon">
                      <a class="facebook" href="https://www.facebook.com/opsicologoonline" title="Facebook" target="_blank"><i class="lni-facebook-filled"></i></a>
                      <a class="twitter" href="https://twitter.com/opsicologonline" title="Twitter" target="_blank"><i class="lni-twitter-filled"></i></a>
                      <a class="instagram" href="https://www.instagram.com/opsicologoonline/" title="Instagram" target="_blank"><i class="lni-instagram-filled"></i></a>
                      <a class="instagram" href="https://plus.google.com/+OpsicologoonlineBrpsi" title="Google+" target="_blank"><i class="lni-google-plus"></i></a>
                      <a class="instagram" href="https://www.youtube.com/c/OpsicologoonlineBrpsi" title="Youtube" target="_blank"><i class="lni-film-play"></i></a>
                  </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                  <h3 class="footer-titel">Sobre a Empresa</h3>
                  <ul>
                      <li><a href="{{ URL::to('/quem-somos') }}" target="_blank">Quem Somos</a></li>
                      <li><a href="{{ URL::to('/perguntas-frequentes') }}" target="_blank">Perguntas Frequentes</a></li>
                      <li><a href="{{ URL::to('/politica-de-privacidade') }}" target="_blank">Politica de Privacidade</a></li>
                      <li><a href="{{ URL::to('/termos-de-uso') }}" target="_blank">Termos de Uso</a></li>
                  </ul>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                  <h3 class="footer-titel">Cadastro</h3>
                  <ul>
                      <li><a href="https://atendimento.opsicologoonline.com.br/sistema/cadastro.php" target="_blank">Sou Psicologo</a></li>
                      <li><a href="https://atendimento.opsicologoonline.com.br/sistema/cadastro.php" target="_blank">Sou Paciente</a></li>
                      <li><a href="https://pages.opsicologoonline.com.br/plataforma-opsicologoonline" target="_blank">PSI Trabalhe Conosco</a></li>
                  </ul>
              </div>
              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                  <h3 class="footer-titel">Suporte</h3>
                  <ul class="menu">
                      <li id="email-de-suporte">suporte@opsicologoonline.com.br</li>
                      <a href="https://api.whatsapp.com/send?phone=5519991449533&text=Tenho%20uma%20d%C3%BAvida%20sobre%20o%20Atendimento%20Online" target="_blank">
                          <div id="li-suporte">
                              <img src="{{ asset('assets/template-principal/img/whatsapp-branco.png') }}" alt="Suporte para psicólogos via whatsapp">
                              <span class="li-suporte-span1">Suporte para Psicólogos</span><br>
                              <span class="li-suporte-span2">Horário de atendimento das 10h às 18h, exceto sábados, domingos e feriados. Responderemos nos dias e horários estabelecidos o mais breve possível</span>
                          </div>
                      </a>
                  </ul>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Footer Section End -->

<section id="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p>Copyright © 2018 O Psicólogo Online - Todos os direitos reservados - Desenvolvido por
            <a href="https://www.facebook.com/victorsetubal" target="_blank">Víctor Setúbal</a> e
            <a href="https://www.linkedin.com/in/auberlansantiago" target="_blank">Santiago Ferraz</a></p>
      </div>
    </div>
  </div>
</section>

<!-- Go to Top Link -->
<a href="#" class="back-to-top">
  <i class="lni-arrow-up"></i>
</a>

<!-- Preloader -->
<div id="preloader">
  <div class="loader" id="loader-1"></div>
</div>
<!-- End Preloader -->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset('assets/js/datepicker/jquery-2.1.1.min.js') }}"></script>
<script src="{{ asset('assets/js/steps_js/jquery.steps.js') }}"></script>
<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/js/additional-methods.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/datepicker/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/js/slick.min.js') }}"></script>
<script src="{{ asset('assets/js/wow.js') }}"></script>
<script src="{{ asset('assets/js/jquery.nav.js') }}"></script>
<script src="{{ asset('assets/js/scrolling-nav.js') }}"></script>
<script src="{{ asset('assets/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.slicknav.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('assets/js/form-validator.min.js') }}"></script>
<script src="{{ asset('assets/js/contact-form-script.min.js') }}"></script>
<script src="{{ asset('assets/js/map.js') }}"></script>
<script src="{{ asset('assets/js/datepicker/moment-with-locales.js') }}"></script>
<script src="{{ asset('assets/js/datepicker/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('assets/js/opsicologoonline.js') }}"></script>
<script src="{{ asset('assets/js/jquery.modal.min.js') }}"></script>



<script src="http://malsup.github.io/jquery.blockUI.js"></script>
{{--<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>--}}
</body>
</html>
