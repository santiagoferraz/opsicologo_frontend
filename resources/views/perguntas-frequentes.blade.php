@extends('estrutura-principal')

@section('css')
    <style>
        .fixed-top {
            background: linear-gradient(to right, #1C7BFF 0%, #00A5FF 100%) !important;
        }
        p {
            text-align: justify !important;
            color: #000 !important;
            font-family: sans-serif !important;
        }
        .section-header .section-title {
            font-size: 20px;
        }
    </style>
@endsection

@section('menu-principal')
    @include('menu-secundario-cabecalho')
@endsection

@section('content')
    <div id="perguntas-frequentes" class="section">
        <div class="container">
            <div class="section-header">
                <p class="btn btn-subtitle wow fadeInDown animated" data-wow-delay="0.2s" style="visibility: visible;-webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">Perguntas Frequentes</p>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <p>
                    <p><strong>O que &eacute; atendimento psicol&oacute;gico online?</strong></p>

                    <p><strong>R:</strong> O atendimento psicol&oacute;gico online &eacute; uma modalidade de atendimento psicol&oacute;gico realizado atrav&eacute;s de recursos online, sendo regulamentado pelo Conselho Federal de Psicologia por meio da Resolu&ccedil;&atilde;o 11/2018, cumprindo e respeitando o C&oacute;digo de &Eacute;tica do Psic&oacute;logo que disciplina a conduta &eacute;tica do psic&oacute;logo, independente do campo de atua&ccedil;&atilde;o.</p>

                    <p>&nbsp;</p>

                    <p><strong>Quais tipos de quest&otilde;es podem ser tratadas no atendimento psicol&oacute;gico online?</strong></p>

                    <p><strong>R:</strong> O atendimento psicol&oacute;gico online &eacute; indicado para as mais diversas situa&ccedil;&otilde;es, podendo estar relacionadas a estresse, ansiedade, sintomas depressivos, inseguran&ccedil;a pessoal, tristeza, solid&atilde;o, luto, medo, conflitos em relacionamentos, conflitos afetivo sexuais, indefini&ccedil;&atilde;o profissional e muitas outras quest&otilde;es, desde que n&atilde;o sejam casos graves. Basta entrar em contato com o psic&oacute;logo e ele ir&aacute; avaliar se o seu caso pode ser acolhido pelo atendimento online.</p>

                    <p>&nbsp;</p>

                    <p><strong>Quem pode utilizar o servi&ccedil;o?</strong></p>

                    <p><strong>R:</strong> O atendimento psicol&oacute;gico online pode ser utilizado por todo indiv&iacute;duo maior de 18 anos, brasileiro ou n&atilde;o, desde que fale o idioma portugu&ecirc;s, residente no Brasil ou em qualquer parte do mundo. Lembrando que no caso de menores de 18 anos ser&aacute; necess&aacute;ria a autoriza&ccedil;&atilde;o expressa dos pais/e ou respons&aacute;vel.</p>

                    <p>&nbsp;</p>

                    <p><strong>Como ser&aacute; a forma de atendimento?</strong></p>

                    <p><strong>R:</strong> O usu&aacute;rio do site O Psic&oacute;logo Online ser&aacute; atendido por meio de videoconfer&ecirc;ncia / chat / &aacute;udio ou via Email. O cliente poder&aacute; escolher ser atendido via videoconfer&ecirc;ncia, &aacute;udio ou apenas pelo chat. Em rela&ccedil;&atilde;o ao atendimento por email, o cliente envia um email com sua queixa para o psic&oacute;logo e tem direito a um email de resposta do mesmo.</p>

                    <p>&nbsp;</p>

                    <p><strong>Qual o valor da sess&atilde;o e quais s&atilde;o as formas de pagamento dispon&iacute;veis?</strong></p>

                    <p><strong>R:</strong> O atendimento psicol&oacute;gico atendimento online, cuja sess&atilde;o tem 50 minutos, sendo realizada via videoconfer&ecirc;ncia / chat / &aacute;udio ou email varia de acordo com cada profissional. Consulte os valores visualizando nossos profissionais. O pagamento pode ser feito por cart&atilde;o de cr&eacute;dito ou boleto banc&aacute;rio. Ao agendar a sess&atilde;o o usu&aacute;rio ser&aacute; encaminhado para efetuar o pagamento.</p>

                    <p>&nbsp;</p>

                    <p><strong>Terei sigilo sobre as informa&ccedil;&otilde;es relatadas ao psic&oacute;logo?</strong></p>

                    <p><strong>R:</strong> O Psic&oacute;logo em qualquer contexto de atua&ccedil;&atilde;o dever&aacute; cumprir e respeitar o c&oacute;digo de &eacute;tica profissional, mantendo o sigilo sobre os dados e quest&otilde;es confiadas a ele no atendimento, sob pena de cometer infra&ccedil;&atilde;o grave ao violar o c&oacute;digo de &eacute;tica dos psic&oacute;logos, al&eacute;m de sofrer san&ccedil;&otilde;es pelos &oacute;rg&atilde;os fiscalizadores da profiss&atilde;o, seu Conselho Regional de Psicologia e Conselho Federal de Psicologia. A preserva&ccedil;&atilde;o da identidade do cliente, tal como a confidencialidade irrestrita das informa&ccedil;&otilde;es obtidas pelo psic&oacute;logo durante o atendimento, s&atilde;o obriga&ccedil;&otilde;es e deveres de todo profissional de psicologia, independente do ambiente onde acontecer o atendimento, aplicando-se as normas ao atendimento online. Entretanto, cabe ressaltar que o atendimento psicol&oacute;gico online n&atilde;o disp&otilde;e de garantias absolutas quando da utiliza&ccedil;&atilde;o de meio eletr&ocirc;nico, por estar este sujeito a viola&ccedil;&otilde;es que fogem do controle e cuidado do profissional. A vulnerabilidade da internet deve ser considerada. Sendo assim &eacute; de fundamental import&acirc;ncia, o cliente certificar-se da seguran&ccedil;a de seu equipamento, instalando programas que detectem poss&iacute;veis amea&ccedil;as, assim como dever&aacute; evitar o atendimento em computadores p&uacute;blicos.</p>

                    <p>&nbsp;</p>

                    <p><strong>Como funciona O Psic&oacute;logo Online?</strong></p>

                    <p>Usar O Psic&oacute;logo Online &eacute; simples, em apenas tr&ecirc;s passos voc&ecirc; j&aacute; estar&aacute; em contato com um psic&oacute;logo.<br />
                        <strong>Passo 1</strong> - Escolha o profissional: Escolha o profissional e veja os hor&aacute;rios dispon&iacute;veis, e clique em avan&ccedil;ar.<br />
                        <strong>Passo 2</strong> - Efetue o Pagamento: Selecione a forma de pagamento e digite os dados necess&aacute;rios.<br />
                        <strong>Passo 3</strong> - Fale com o Psic&oacute;logo em Tempo Real: Ap&oacute;s cadastro finalizado e o pagamento confirmado, voc&ecirc; ter&aacute; direito ao seu atendimento online conforme disponibilizado pelo psic&oacute;logo.</p>

                    <p>&nbsp;</p>

                    <p><strong>Posso acessar O Psic&oacute;logo Online de Smartphone ou Tablet?</strong><br />
                        <strong>R:</strong> Sim voc&ecirc; pode ser atendido pelo Smartphone ou Tablet, por&eacute;m para um melhor aproveitamento no atendimento do recomendamos o uso de computadores de mesa ou notebooks.</p>


                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection