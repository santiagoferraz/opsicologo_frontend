@extends('estrutura-principal')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.modal.min.css') }}" >
<style>
    .error
    {
        color: #ff3111;
        border-color: #ff3111;
    }
</style>
@endsection

@section('menu-principal')
    @include('menu-principal-cabecalho')
@endsection

@section('carousel-area')
    <div id="carousel-area">
        <div id="carousel-slider" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-slider" data-slide-to="1" class=""></li>
                <li data-target="#carousel-slider" data-slide-to="2" class=""></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item carousel-agency active">
                    <img src="{{ asset('assets/template-principal/img/slider/img-1.jpg') }}" alt="">
                    <div class="overlay"></div>
                    <div class="carousel-caption text-left">
                        <h2 class="wow fadeInRight animated" data-wow-delay="0.4s" style="visibility: visible;-webkit-animation-delay: 0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">
                            Atendimento psicológico <br>onde você estiver</h2>
                        <p class="wow fadeInRight animated" data-wow-delay="0.6s" style="visibility: visible;-webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
                            Consultas com psicólogo por videoconferência, chat, áudio e/ou e-mail na data e horário que vocês escolher.</p>
                        <a href="#services" class="btn btn-lg btn-common btn-effect wow fadeInRight animated page-scroll" data-wow-delay="0.9s" style="visibility: visible;-webkit-animation-delay: 0.9s; -moz-animation-delay: 0.9s; animation-delay: 0.9s;">
                            Agendar Consulta</a>
                    </div>
                </div>
                <div class="carousel-item carousel-agency">
                    <img src="{{ asset('assets/template-principal/img/slider/img-2.jpg') }}" alt="">
                    <div class="overlay"></div>
                    <div class="carousel-caption text-right">
                        <h2 class="wow fadeInLeft animated" data-wow-delay="0.6s" style="visibility: visible;-webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
                            Os melhores especialistas <br>em psicologia</h2>
                        <p class="wow fadeInUp animated" data-wow-delay="0.9s" style="visibility: visible;-webkit-animation-delay: 0.9s; -moz-animation-delay: 0.9s; animation-delay: 0.9s;">
                            Selecionamos e filtramos os melhores profissionais do ramo de psicologia <br>para seu melhor aproveitamento e experiência no atendimento.</p>
                        <a href="#blog" class="btn btn-lg btn-border-filled wow fadeInUp animated page-scroll" data-wow-delay="1.2s" style="visibility: visible;-webkit-animation-delay: 1.2s; -moz-animation-delay: 1.2s; animation-delay: 1.2s;">
                            Conhecer</a>
                    </div>
                </div>
                <div class="carousel-item carousel-agency">
                    <img src="{{ asset('assets/template-principal/img/slider/img-3.jpg') }}" alt="">
                    <div class="overlay"></div>
                    <div class="carousel-caption text-left">
                        <h2 class="wow fadeInRight animated" data-wow-delay="0.6s" style="visibility: visible;-webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
                            Torne-se quem você quer ser</h2>
                        <p class="wow fadeInUp animated" data-wow-delay="0.6s" style="visibility: visible;-webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
                            Com a orientação adequada você poderá ir muito longe. <br/>Conquiste, ouse, realize-se, doe-se á vida.</p>
                        <a href="#services" class="btn btn-lg btn-border wow fadeInUp animated page-scroll" data-wow-delay="0.9s" style="visibility: visible;-webkit-animation-delay: 0.9s; -moz-animation-delay: 0.9s; animation-delay: 0.9s;">
                            Começar Agora</a>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carousel-slider" role="button" data-slide="prev">
                <span class="carousel-control" aria-hidden="true"><i class="lni-chevron-left"></i></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-slider" role="button" data-slide="next">
                <span class="carousel-control" aria-hidden="true"><i class="lni-chevron-right"></i></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
@endsection

@section('content')

    <!-- features Section Start -->
    <div id="app-features" class="section">
        <div class="container">
            <div class="section-header">
                <p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">vantagens</p>
                <div>
                    <img class="img-vantagens wow zoomIn" data-wow-delay="0.2s" src="{{ asset('assets/template-principal/img/logo_g.png') }}" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-12 col-xs-12">
                    <div class="content-left text-right">
                        <div class="box-item left">
                        <span class="icon">
                          <i class="lni-bubble"></i>
                        </span>
                            <div class="text">
                                <h4>Acessível</h4>
                                <p>Acesso e atendimento de qualquer dispositivo, onde e quando você quiser.</p>
                            </div>
                        </div>
                        <div class="box-item left">
                        <span class="icon">
                          <i class="lni-star-filled"></i>
                        </span>
                            <div class="text">
                                <h4>Profissional</h4>
                                <p>Os psicólogos mais capacitados com excelência em atendimento.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-xs-12">
                    <div class="show-box">
                        <img src="{{ asset('assets/template-principal/img/responsividade.png') }}" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-xs-12">
                    <div class="content-right text-left">
                        <div class="box-item right">
                        <span class="icon">
                          <i class="lni-shield"></i>
                        </span>
                            <div class="text">
                                <h4>Seguro</h4>
                                <p>Cryptografia de acesso e sigilo absoluto dos dados e informações.</p>
                            </div>
                        </div>
                        <div class="box-item right">
                        <span class="icon">
                          <i class="lni-wallet"></i>
                        </span>
                            <div class="text">
                                <h4>Econômico</h4>
                                <p>Melhor custo/benefício com valores que cabem no seu bolso.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- features Section End -->

    <!-- Start Video promo Section -->
    <section class="video-promo section">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="video-promo-content text-center">
                        <h2 class="mt-3 wow zoomIn" data-wow-duration="1000ms" data-wow-delay="100ms">Vídeo Apresentação</h2>
                        <p>Saiba mais sobre atendimento psicológico online</p>
                        <a href="https://www.youtube.com/watch?v=Cw63QXvF6iA" class="video-popup"><i class="lni-film-play"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Video Promo Section -->

    <!-- Services Section Start -->
    <section id="services" class="feature-content section">
        <div class="container">
            <div class="section-header">
                <p class="btn btn-subtitle wow fadeInDown animated" data-wow-delay="0.2s" style="visibility: visible;-webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">Agendar Consulta</p>
                <h2 class="section-title">Em poucos passos você consegue agendar uma consulta</h2>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="passos-agendamento single-feature">
                        <i class="lni-user"></i>
                        <h4>escolha o psicólogo</h4>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="passos-agendamento single-feature">
                        <i class="lni-calendar"></i>
                        <h4>escolha o dia e horário</h4>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="passos-agendamento single-feature">
                        <i class="lni-check-mark-circle"></i>
                        <h4>efetue o pagamento</h4>
                    </div>
                </div>
            </div>
            <div id="btn-passsos-agendamento">
                <a href="#blog" class="btn btn-common btn-effect page-scroll">Escolher Psicólogo</a>
            </div>
        </div>
    </section>
    <!-- Services Section End -->

    <!-- Blog section Start -->
    <section id="blog" class="section">
        <div class="container">
            <div class="section-header">
                <p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">Especialistas</p>
                <h2 class="section-title wow zoomIn" data-wow-delay="0.2s">Psicólogos em Destaque</h2>
            </div>
            <div class="row" id="grid-list-psicologos-destaque"></div>
            <div class="section-title" id="carregando-destaques" style="text-align: center">
                <img src="{{ asset('assets/img/carregando-medio.gif') }}" alt="">
            </div>
            <div id="btn-mais-psicologos">
                <a href="{{ URL::to('psicologo/lista-de-psicologos') }}" class="class='btn-mais-detalhes btn btn-common btn-effect'">
                    <i class="lni-reload"></i> Mais Psicólogos</a>
            </div>
        </div>
    </section>
    <!-- Blog section End -->

    <!-- Testimonial Section Start -->
    <section id="testimonial" class="section">
        <div class="container">
            <div class="section-header">
                <p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">Depoimentos</p>
                <h2 class="section-title wow zoomIn" data-wow-delay="0.2s">O que dizem sobre nós</h2>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div id="testimonials" class="touch-slider owl-carousel">
                        @foreach(App\Enums\Depoimentos::DEPOIMENTOS as $depoimento)
                            <div class="item">
                                <div class="testimonial-item">
                                    <div class="author">
                                        <div class="img-thumb">
                                            <img src="{{ App\Enums\Depoimentos::URL_IMG . $depoimento['img'] }}" alt="">
                                        </div>
                                    </div>
                                    <div class="content-inner">
                                        <p class="description">{{ $depoimento['texto'] }}</p>
                                        <div class="author-info">
                                            <h2>- {{ $depoimento['nome'] }}</h2>
                                            <span>{{ $depoimento['tipo'] }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->

    <!-- Subcribe Section Start -->
    <div id="subscribe" class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7 col-md-12 col-xs-12">
                    <div class="subscribe-form">
                        <div class="form-wrapper">
                            <div class="sub-title text-center">
                                <h3>Inscreva-se</h3>
                                <p>Receba artigos, novidades, dicas e muitos mais no seu e-mail.</p>
                            </div>
                            <form id="formInscricao">
                                <div class="row">
                                    <div class="col-12 form-line">
                                        <div class="form-group form-search">
                                            <input type="email" id="emailInscricao" class="form-control" name="emailInscricao" placeholder="Digite Seu E-mail">
                                            <input type="button" id="inscricaoLeadLovers" class="btn btn-common btn-search" value="Inscrever"/>
                                            <div class="sub-title text-center" id="carregando-inscricao" style="margin-top: 20px">
                                                <p>Por favor, aguarde! Enviando inscrição.</p>
                                                <img height="50px" src="{{ asset('assets/img/carregando-circle.svg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Subcribe Section End -->

    <!-- Contact Section Start -->
    <section id="contact">
        <div class="contact-form">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="offset-top">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="contact-block wow fadeInUp" data-wow-delay="0.2s">
                                <div class="section-header">
                                    <p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">Fale Conosco</p>
                                    <h2 class="section-title wow zoomIn" data-wow-delay="0.2s">Formulário de Contato</h2>
                                </div>
                                <form id="contactForm">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control required" id="nomeRemetente" maxlength="500" name="nomeRemetente" placeholder="Nome" data-error="Por favor, informe seu nome">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="email" placeholder="E-mail" id="emailRemetente" maxlength="500" class="form-control required" name="emailRemetente" data-error="Por favor, informe seu e-mail">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" placeholder="Assunto" id="assunto"  maxlength="500" class="form-control" data-error="Por favor, informe o assunto">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea class="form-control required" name="mensagemFaleConosco" id="mensagem" maxlength="5000" placeholder="Mensagem" rows="7" data-error="Por favor, digite a mensagem"></textarea>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="submit-button">
                                    <div id="enviando-fale-conosco" style="text-align: center">
                                        <img src="{{ asset('assets/img/carregando-medio.gif') }}" alt="">
                                    </div>
                                    <button class="btn btn-common btn-effect" id="enviarFaleConosco">Enviar</button>
                                    <div id="msgSubmit" class="h3 hidden"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="modalFaleConosco" style="overflow: visible">
       <div id="conteudoFaleConosco">
       </div>
        <a href="#" rel="modal:close"></a>
    </div>

    <div id="modalLeadLovers" style="overflow: visible">
        <a href="#" rel="modal:close"></a>
    </div>
    <!-- Contact Section End -->

@endsection


@section('js')
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/additional-methods.min.js') }}"></script>
    <script src="{{ asset('assets/js/portfolio.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.modal.min.js') }}"></script>

@endsection