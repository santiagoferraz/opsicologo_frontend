@extends('estrutura-principal')

@section('css')
    <style>
        .fixed-top {
            background: linear-gradient(to right, #1C7BFF 0%, #00A5FF 100%) !important;
        }
        p {
            text-align: justify !important;
            color: #000 !important;
            font-family: sans-serif !important;
        }
        .section-header .section-title {
            font-size: 20px;
        }
    </style>
@endsection

@section('menu-principal')
    @include('menu-secundario-cabecalho')
@endsection

@section('content')
    <div id="politica-de-privacidade" class="section">
        <div class="container">
            <div class="section-header">
                <p class="btn btn-subtitle wow fadeInDown animated" data-wow-delay="0.2s" style="visibility: visible;-webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">Politica de Privacidade</p>
                <h2 class="section-title wow zoomIn" data-wow-delay="0.2s">Esta Política de Privacidade foi formulada com o intuito de manter a privacidade e a segurança das informações coletadas de nossos clientes e usuários.</h2>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <p>
                        Política de privacidade O Psicólogo Online
                        Todas as suas informações pessoais recolhidas, serão usadas para ajudar a tornar a sua visita no nosso site o mais produtiva e agradável possível.
                        <br><br>
                        A garantia da confidencialidade dos dados pessoais dos utilizadores do nosso site é importante para o O Psicólogo Online.
                        <br><br>
                        Todas as informações pessoais relativas a membros, assinantes, clientes ou visitantes que usem o a plataforma O Psicólogo Online serão tratadas em concordância com a Lei da Proteção de Dados Pessoais de 26 de outubro de 1998 (Lei n.º 67/98).
                        <br><br>
                        A informação pessoal recolhida pode incluir o seu nome, e-mail, número de telefone e/ou telemóvel, morada, data de nascimento e/ou outros.
                        <br><br>
                        O uso da plataforma O Psicólogo Online pressupõe a aceitação deste Acordo de privacidade. A equipe da plataforma O Psicólogo Online reserva-se ao direito de alterar este acordo sem aviso prévio. Deste modo, recomendamos que consulte esta página com regularidade de forma a estar sempre atualizado.
                        <br><br>
                        Os anúncios
                        Tal como outros websites, coletamos e utilizamos informação contida nos anúncios. A informação contida nos anúncios, inclui o seu endereço IP (Internet Protocol), o seu ISP (Internet Service Provider, como o Sapo, Clix, ou outro), o browser que utilizou ao visitar o nosso website (como o Internet Explorer ou o Firefox), o tempo da sua visita e que páginas visitou dentro do nosso website.
                        <br><br>
                        Os Cookies e Web Beacons
                        Utilizamos cookies para armazenar informação, tais como as suas preferências pessoas quando visita o nosso website. Isto poderá incluir um simples popup, ou uma ligação em vários serviços que providenciamos, tais como fóruns.
                        <br><br>
                        Em adição também utilizamos publicidade de terceiros no nosso website para suportar os custos de manutenção. Alguns destes publicitários, poderão utilizar tecnologias como os cookies e/ou web beacons quando publicitam no nosso website, o que fará com que esses publicitários (como o Google através do Google AdSense) também recebam a sua informação pessoal, como o endereço IP, o seu ISP, o seu browser, etc. Esta função é geralmente utilizada para geotargeting (mostrar publicidade de Lisboa apenas aos leitores oriundos de Lisboa por ex.) ou apresentar publicidade direcionada a um tipo de utilizador (como mostrar publicidade de restaurante a um utilizador que visita sites de culinária regularmente, por ex.).
                        <br><br>
                        Você detém o poder de desligar os seus cookies, nas opções do seu browser, ou efetuando alterações nas ferramentas de programas Anti-Virus, como o Norton Internet Security. No entanto, isso poderá alterar a forma como interage com o nosso website, ou outros websites. Isso poderá afetar ou não permitir que faça logins em programas, sites ou fóruns da nossa e de outras redes.
                        <br><br>
                        Ligações a Sites de terceiros
                        O Psicólogo Online possui ligações para outros sites, os quais, a nosso ver, podem conter informações / ferramentas úteis para os nossos visitantes. A nossa política de privacidade não é aplicada a sites de terceiros, pelo que, caso visite outro site a partir do nosso deverá ler a politica de privacidade do mesmo.
                        <br><br>
                        Não nos responsabilizamos pela política de privacidade ou conteúdo presente nesses mesmos sites.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection