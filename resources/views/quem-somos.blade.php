@extends('estrutura-principal')

@section('css')
    <style>
        .fixed-top {
            background: linear-gradient(to right, #1C7BFF 0%, #00A5FF 100%) !important;
        }
        p {
            text-align: justify !important;
            color: #000 !important;
            font-family: sans-serif !important;
        }
        .section-header .section-title {
            font-size: 20px;
        }
    </style>
@endsection

@section('menu-principal')
    @include('menu-secundario-cabecalho')
@endsection

@section('content')
    <div id="quem-somos" class="section">
        <div class="container">
            <div class="section-header">
                <p class="btn btn-subtitle wow fadeInDown animated" data-wow-delay="0.2s" style="visibility: visible;-webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">Quem Somos</p>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <p>
                        O Psicologo Online é uma plataforma online que permite conectar psicólogos e pacientes onde quer que eles estejam e a qualquer horário com segurança e qualidade.
                        <br><br>
                        O serviço pode ser utilizado por adultos, casais e adolescentes.
                        <br><br>
                        <b>Obs:</b> Para menores de 18 anos é necessária autorização por escrito dos pais ou responsáveis. (Deve ser impressa a autorização disponível no site, assinada, escaneada e enviada para: <b>contato@opsicologoonline.com.br</b>)
                        <br>
                        » Clique para baixar a autorização
                        <br><br>
                        Este é um serviço único que prioriza não só a qualidade técnica, mas também a melhor prática da Psicologia, respeitando a ética e a sua legislação existente.
                        <br><br>
                        <b>Quais as Vantagens do Atendimento Online?</b>
                        <br><br>
                        Permite um contato flexível (de acordo com as necessidades do cliente, você escolhe o dia e a hora);
                        <br><br>
                        Por poder ser atendido no conforto de seu lar, você pode ter maior estímulo e desinibição para expor seus pensamentos;
                        <br><br>
                        A consulta online, assim como a presencial, é confidencial, nós do site O Psicólogo Online seguimos a risca a ética profissional;
                        <br><br>
                        Tem as mesmas obrigações éticas inerentes ao exercício profissional do psicólogo: <b>privacidade e confidencialidade</b>;
                        <br><br>
                        Possibilidade do cliente entrar em contato e contratar os serviços de profissionais de onde estiver, a hora que quiser;
                        <br><br>
                        Se na sua região você tem dificuldades em encontrar um Psicólogo, aqui isso pode ser resolvido rapidamente;
                        <br><br>
                        Pessoas que tem dificuldade de locomoção.
                        <br><br>
                        <b>Sinta a diferença</b>
                        <br><br>
                        Qualidade
                        Segurança
                        Melhor prática da Pscicologia
                        Ética
                        Legislação
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection