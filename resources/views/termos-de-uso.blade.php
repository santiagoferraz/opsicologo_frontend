@extends('estrutura-principal')

@section('css')
    <style>
        .fixed-top {
            background: linear-gradient(to right, #1C7BFF 0%, #00A5FF 100%) !important;
        }
        p {
            text-align: justify !important;
            color: #000 !important;
            font-family: sans-serif !important;
        }
        .section-header .section-title {
            font-size: 20px;
        }
        .pl-30 {
            padding-left: 30px;
        }
        .pl-60 {
            padding-left: 60px;
        }
    </style>
@endsection

@section('menu-principal')
@include('menu-secundario-cabecalho')
@endsection

@section('content')
    <div id="termos-de-uso" class="section">
        <div class="container">
            <div class="section-header">
                <p class="btn btn-subtitle wow fadeInDown animated" data-wow-delay="0.2s" style="visibility: visible;-webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">TERMOS E CONDI&Ccedil;&Otilde;ES DE USO PARA PSIC&Oacute;LOGOS</p>
                {{--<h2 class="section-title wow zoomIn" data-wow-delay="0.2s">Esta Declara&ccedil;&atilde;o de Direitos e Responsabilidades ("Declara&ccedil;&atilde;o", "Termos" ou "DDR") exp&otilde;e os termos de servi&ccedil;o que determinam nosso relacionamento com os psic&oacute;logos, pacientes e quaisquer outras pessoas que de qualquer forma interagem com a Plataforma O Psic&oacute;logo Online (Plataforma OPO), bem como marcas, produtos e servi&ccedil;os do site O Psic&oacute;logo Online.</h2>--}}
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <p>Esta Declara&ccedil;&atilde;o de Direitos e Responsabilidades ("Declara&ccedil;&atilde;o", "Termos" ou "DDR") exp&otilde;e os termos de servi&ccedil;o que determinam nosso relacionamento com os psic&oacute;logos, pacientes e quaisquer outras pessoas que de qualquer forma interagem com a Plataforma O Psic&oacute;logo Online (Plataforma OPO), bem como marcas, produtos e servi&ccedil;os do site O Psic&oacute;logo Online.</p>
                    <p>Defini&ccedil;&atilde;o: A Plataforma O Psic&oacute;logo Online permite conectar psic&oacute;logos e pacientes por meio de uma plataforma tecnol&oacute;gica online onde quer que eles estejam e em qualquer hor&aacute;rio, com &eacute;tica profissional, qualidade e seguran&ccedil;a.</p>
                    <p>Ao usar ou acessar os Servi&ccedil;os da Plataforma O Psic&oacute;logo Online voc&ecirc; concorda publicamente e de forma legal com todo o conte&uacute;do expresso nesta Declara&ccedil;&atilde;o.</p>
                    <p>Voc&ecirc; atesta estar ciente que as delibera&ccedil;&otilde;es da resolu&ccedil;&atilde;o 11/2018 do Conselho Federal de Psicologia sobre os limites do atendimento online s&atilde;o:</p>
                    <p class="pl-30">Art. 2o - S&atilde;o autorizadas a presta&ccedil;&atilde;o dos seguintes servi&ccedil;os psicol&oacute;gicos realizados por meios tecnol&oacute;gicos da informa&ccedil;&atilde;o e comunica&ccedil;&atilde;o, desde que n&atilde;o firam as disposi&ccedil;&otilde;es do C&oacute;digo de &Eacute;tica Profissional da psic&oacute;loga e do psic&oacute;logo a esta Resolu&ccedil;&atilde;o:</p>
                    <p class="pl-60">I. As consultas e/ou atendimentos psicol&oacute;gicos de diferentes tipos de maneira s&iacute;ncrona ou ass&iacute;ncrona;</p>
                    <p class="pl-60">II. Os processos de Sele&ccedil;&atilde;o de Pessoal;</p>
                    <p class="pl-60">III. Utiliza&ccedil;&atilde;o de instrumentos psicol&oacute;gicos devidamente regulamentados por resolu&ccedil;&atilde;o pertinente, sendo que os testes psicol&oacute;gicos devem ter parecer favor&aacute;vel do Sistema de Avalia&ccedil;&atilde;o de Instrumentos Psicol&oacute;gicos (SATEPSI), com padroniza&ccedil;&atilde;o e normatiza&ccedil;&atilde;o espec&iacute;fica para tal finalidade.</p>
                    <p class="pl-60">IV. A supervis&atilde;o t&eacute;cnica dos servi&ccedil;os prestados por psic&oacute;logas e psic&oacute;logos nos mais diversos contextos de atua&ccedil;&atilde;o.</p>
                    <p class="pl-30">&sect; 1o. - Entende-se por consulta e/ou atendimentos psicol&oacute;gicos o conjunto sistem&aacute;tico de procedimentos, por meio da utiliza&ccedil;&atilde;o de m&eacute;todos e t&eacute;cnicas psicol&oacute;gicas do qual se presta um servi&ccedil;o nas diferentes &aacute;reas de atua&ccedil;&atilde;o da Psicologia com vistas &agrave; avalia&ccedil;&atilde;o, orienta&ccedil;&atilde;o e/ou interven&ccedil;&atilde;o em processos individuais e grupais.</p>
                    <p><strong>Defini&ccedil;&atilde;o dos termos</strong></p>
                    <p>A Plataforma O Psic&oacute;logo Online &eacute; um site de apoio ao Psic&oacute;logo e ao Paciente que tem por objetivos ampliar e viabilizar o atendimento psicol&oacute;gico atrav&eacute;s do uso da Internet em ambiente completamente seguro e dentro dos par&acirc;metros exigidos pelo Conselho Federal de Psicologia.</p>
                    <p>A Plataforma O Psic&oacute;logo Online n&atilde;o &eacute; respons&aacute;vel e de maneira alguma se responsabilizar&aacute; pelo conte&uacute;do e forma dos atendimentos online ou presenciais prestados pelos psic&oacute;logos cadastrados, assumindo apenas o papel de intermedi&aacute;rio entre profissional e clientes que buscam por servi&ccedil;os profissionais.</p>
                    <p>S&atilde;o de total responsabilidade dos profissionais psic&oacute;logos suas condutas &eacute;ticas e o cumprimento da resolu&ccedil;&atilde;o 11/2018 do conselho federal de psicologia e o n&atilde;o cumprimento dessas determina&ccedil;&otilde;es pode acarretar no desligamento do profissional da Plataforma.</p>
                    <p><strong>Condi&ccedil;&otilde;es restritivas de uso</strong></p>
                    <p>Apenas os profissionais registrados junto ao CRP de suas regi&otilde;es ser&atilde;o e que est&atilde;o em situa&ccedil;&atilde;o regular referente a &eacute;tica profissional ser&atilde;o aceitos na Plataforma OPO.</p>
                    <p>E para os profissionais que forem oferecer os servi&ccedil;os de atendimentos psicol&oacute;gicos online, estes devem estar devidamente cadastrados e aprovados para oferecer atendimentos e servi&ccedil;os por meio de Tecnologias de Informa&ccedil;&atilde;o e Comunica&ccedil;&atilde;o (TICs) no E-psi.</p>
                    <p>A Plataforma OPO n&atilde;o &eacute; respons&aacute;vel por fiscalizar e/ou regularizar a situa&ccedil;&atilde;o do psic&oacute;logo junto ao seu CRP e junto ao site E-psi, sendo de total responsabilidade do mesmo estar sempre com situa&ccedil;&atilde;o regular.</p>
                    <p>Caso haja alguma situa&ccedil;&atilde;o de delitos &eacute;ticos por parte de qualquer profissional cadastrado na Plataforma OPO disponibilizamos o e-mail <a href="mailto:suporte@opsicologoonline.com.br"><span style="color: #1155cc;"><u>suporte@opsicologoonline.com.br</u></span></a> para que possamos averiguar o caso.</p>
                    <p><strong>Direitos Autorais e de Imagem </strong></p>
                    <p>&Eacute; estritamente proibido gravar qualquer atendimento ou sess&atilde;o, seja por meio de grava&ccedil;&atilde;o onde apare&ccedil;am imagens, sons, textos, ou outra linguagem criptografada ou n&atilde;o, ficando expressamente proibido de veicular quaisquer grava&ccedil;&otilde;es das sess&otilde;es, incorrendo em crime de uso indevido da imagem e voz do psic&oacute;logo e do Paciente/Cliente.</p>
                    <p><strong>Dos servi&ccedil;os oferecidos</strong></p>
                    <p>O atendimento psicol&oacute;gico online pode ser utilizado por todo indiv&iacute;duo maior de 18 anos, brasileiro ou n&atilde;o, desde que fale o idioma portugu&ecirc;s, residente no Brasil ou em qualquer parte do mundo e que concordem com esses termos e condi&ccedil;&otilde;es de uso.</p>
                    <p>Lembrando que no caso de menores de 18 anos ser&aacute; necess&aacute;ria a autoriza&ccedil;&atilde;o expressa dos pais/e ou respons&aacute;vel.</p>
                    <p>Somente ser&atilde;o realizadas consultas/atendimentos psicol&oacute;gicas a pacientes maiores de 18 anos, com o consentimento expresso de ao menos um dos respons&aacute;veis legais e mediante avalia&ccedil;&atilde;o de viabilidade t&eacute;cnica por parte da psic&oacute;loga e do psic&oacute;logo para a realiza&ccedil;&atilde;o desse tipo de servi&ccedil;o&rdquo; (Art. 5o resolu&ccedil;&atilde;o 11/2108)</p>
                    <p><strong>Para os servi&ccedil;os de atendimento psicol&oacute;gicos online, alguns s&atilde;o inadequados enquanto outros s&atilde;o vedados pelo Conselho Federal de Psicologia cabendo ao profissional fazer a avalia&ccedil;&atilde;o se h&aacute; possibilidade da presta&ccedil;&atilde;o do servi&ccedil;o.</strong></p>
                    <p>&ldquo;O atendimento de pessoas e grupos em situa&ccedil;&atilde;o de urg&ecirc;ncia e emerg&ecirc;ncia pelos meios de tecnologia e informa&ccedil;&atilde;o (...) &eacute; inadequado, devendo a presta&ccedil;&atilde;o desse tipo de servi&ccedil;o ser executado por profissionais e equipes de forma presencial.&rdquo; (Art. 6&deg;. Resolu&ccedil;&atilde;o 11/2018)</p>
                    <p>&ldquo;O atendimento de pessoas e grupos em situa&ccedil;&atilde;o de emerg&ecirc;ncia e desastres pelos meios detecnologia e informa&ccedil;&atilde;o (...) &eacute; vedado, devendo a presta&ccedil;&atilde;o desse tipo de servi&ccedil;o ser executado por profissionais e equipes de forma presencial.&rdquo; (Art. 7&deg;. Resolu&ccedil;&atilde;o 11/2018)</p>
                    <p>&ldquo;&Eacute; vedado o atendimento de pessoas e grupos em situa&ccedil;&atilde;o de viola&ccedil;&atilde;o de direitos ou de viol&ecirc;ncia, pelos meios de tecnologia e informa&ccedil;&atilde;o (...), devendo a presta&ccedil;&atilde;o desse tipo de servi&ccedil;o ser executado por profissionais e equipes de forma presencial.&rdquo; (Art. 8&deg;. Resolu&ccedil;&atilde;o 11/2018)</p>
                    <p><strong>Sigilo e Confiabilidade</strong></p>
                    <p>Em conformidade com o C&oacute;digo de &Eacute;tica, todas as sess&otilde;es prestadas pelos profissionais da Plataforma OPO devem mant&eacute;m o sigilo profissional, e jamais qualquer informa&ccedil;&atilde;o ser&aacute; compartilhada com outras pessoas, institui&ccedil;&otilde;es ou na internet e as sess&otilde;es jamais ser&atilde;o gravadas ou armazenadas na Plataforma OPO.</p>
                    <p>Por&eacute;m &eacute; v&aacute;lido ressaltar que os servi&ccedil;os mediados por tecnologias da informa&ccedil;&atilde;o n&atilde;o podem em hip&oacute;tese alguma serem considerados como totalmente seguros e sigilosos, pois s&atilde;o vulner&aacute;veis a amea&ccedil;as digitais.</p>
                    <p>Aconselha-se a n&atilde;o usar computadores p&uacute;blicos e apagar os hist&oacute;ricos de conversa&ccedil;&otilde;es sempre ap&oacute;s as sess&otilde;es. &Eacute; tamb&eacute;m importante proteger o seu computador com um programa de antiv&iacute;rus e firewall.</p>
                    <p><strong>Valores e formas de pagamento</strong></p>
                    <p>Os valores e tempo de sess&atilde;o variam de acordo com cada profissional. A Plataforma O Psic&oacute;logo Online n&atilde;o recebe pelas consultas e n&atilde;o faz repasse de valores para os profissionais. Cada profissional receber&aacute; o valor de sua consulta diretamente em sua conta. O pagamento por parte dos usu&aacute;rios podem ser feitos por cart&atilde;o de cr&eacute;dito, dep&oacute;sito, transfer&ecirc;ncia ou boleto banc&aacute;rio. Ao agendar a sess&atilde;o o usu&aacute;rio ser&aacute; encaminhado para efetuar o pagamento ou o mesmo poder&aacute; entrar em contato com o profissional para combinar outras formas de pagamento e valores.</p>
                    <p>O Psic&oacute;logo Online se compromete a oferecer os seguintes servi&ccedil;os aos clientes:</p>
                    <p>Consult&oacute;rio Virtual: Sistema de agendamento e pagamento de consultas com notifica&ccedil;&otilde;es via e-mail que possibilita que os psic&oacute;logos e pacientes se comuniquem onde quer que estejam e em qualquer hor&aacute;rio.</p>
                    <p><strong>Apoio no Pagamento e Recebimento dos Servi&ccedil;os:</strong></p>
                    <p>O Psic&oacute;logo Online disponibiliza em seu sistema configura&ccedil;&otilde;es adequadas para que os profissionais recebem atrav&eacute;s de ferramentas de pagamento online consolidadas no mercado como o PagSeguro ou Paypall. O paciente paga a sua consulta para o atendimento online de acordo com o valor estipulado pelo Psic&oacute;logo, sendo que n&atilde;o ser&aacute; cobrado do psic&oacute;logo percentual sobre sua consulta.</p>
                    <p><strong>Suporte t&eacute;cnico</strong></p>
                    <p>A Plataforma possui suporte t&eacute;cnico gratuito para resolver eventuais problemas, orientando tanto os psic&oacute;logos como os Pacientes, atrav&eacute;s dos n&uacute;meros telef&ocirc;nicos divulgados e atrav&eacute;s de e-mail contato@opsicologoonline.com.br.</p>
                    <p><strong>Sistema de busca</strong></p>
                    <p>A p&aacute;gina de profissionais cadastrados na Plataforma possui um sistema de busca que permite ao Pacientes localizar um psic&oacute;logo que atenda &agrave;s suas necessidades atrav&eacute;s do nome, estado ou cidade do mesmo.</p>
                    <p><strong>Cancelamento e devolu&ccedil;&atilde;o do valor pago</strong></p>
                    <p>O pagamento da consulta/atendimento psicol&oacute;gico &eacute; feito antes do agendamento da sess&atilde;o. Caso o paciente desista, ele poder&aacute; solicitar a devolu&ccedil;&atilde;o dos valores at&eacute; 24 horas antes do atendimento. Caso contr&aacute;rio, o valor n&atilde;o ser&aacute; devolvido.</p>
                    <p>Ser&aacute; descontado do valor a ser devolvido, todas as taxas que foram cobradas na transa&ccedil;&atilde;o financeira. Caso queira solicitar a devolu&ccedil;&atilde;o dos valores pagos, o cliente deve entrar em contato diretamente com o profissional no qual agendou a consulta e fez o pagamento.</p>
                    <p><strong>Problemas T&eacute;cnicos</strong></p>
                    <p>Caso ocorra algum problema t&eacute;cnico na Plataforma O Psic&oacute;logo Online que prejudiquem a qualidade ou at&eacute; a mesmo a impossibilidade da realiza&ccedil;&atilde;o do atendimento, a sess&atilde;o poder&aacute; ser reagendada sem nenhum &ocirc;nus para o usu&aacute;rio.</p>
                    <p>Vale ressaltar que problemas t&eacute;cnicos apesar de n&atilde;o serem comuns podem acontecer e tanto psic&oacute;logos e pacientes estando cientes disso devem esperar os prazos estipulados pelo desenvolvedor para que os problemas sejam corrigidos.</p>
                    <p><strong>N&atilde;o comparecimento do paciente</strong></p>
                    <p>O psic&oacute;logo escolhido pelo cliente disponibiliza o dia e a hora para o atendimento do mesmo e fica aguardando no hor&aacute;rio da sess&atilde;o pelo tempo contratado pelo cliente. Caso o cliente n&atilde;o compare&ccedil;a nos dias e hor&aacute;rios marcados, independente dos motivos, o valor da sess&atilde;o n&atilde;o ser&aacute; devolvido. Caso n&atilde;o possa comparecer o cliente deve cancelar ou reagendar a sess&atilde;o com at&eacute; 24h de anteced&ecirc;ncia.</p>
                    <p><strong>Mudan&ccedil;a no hor&aacute;rio da sess&atilde;o</strong></p>
                    <p>Caso o cliente deseje alterar o hor&aacute;rio da sess&atilde;o, dever&aacute; fazer isso pelo menos 24 horas antes do in&iacute;cio da sess&atilde;o marcada. O cliente deve cancelar a sess&atilde;o que havia agendado com 24 horas de anteced&ecirc;ncia e em seguida agendar nova sess&atilde;o nos dias e hor&aacute;rios desejados. N&atilde;o ser&aacute; poss&iacute;vel remarcar a sess&atilde;o se isso n&atilde;o for feito com pelo menos 24h de anteced&ecirc;ncia e o cliente perder&aacute; o direito de reagendar a sess&atilde;o.</p>
                    <p><strong>Do acesso a internet e conex&atilde;o</strong></p>
                    <p>A Plataforma O Psic&oacute;logo Online n&atilde;o &eacute; respons&aacute;vel pelo acesso a internet do psic&oacute;logo e do usu&aacute;rio, sendo o acesso de total responsabilidade dos mesmos. A Plataforma O Psic&oacute;logo Online se exime de qualquer responsabilidade quanto &agrave; indisponibilidade gerada por problemas t&eacute;cnicos do computador do psic&oacute;logo e usu&aacute;rio ou de conex&atilde;o do psic&oacute;logo e usu&aacute;rio e o impedimento de acesso &agrave; rede.</p>
                    <p>Para uma boa experi&ecirc;ncia de atendimento, &eacute; importante que possua uma boa conex&atilde;o com a internet, para que a sess&otilde;es por v&iacute;deo, &aacute;udio ou chat sejam realizadas sem problemas.</p>
                    <p>O site opsicologoonline.com.br visando a melhoria na qualidade dos servi&ccedil;os prestados, se reserva no direito de efetuar eventuais manuten&ccedil;&otilde;es em seu sistema. O psic&oacute;logo ser&aacute; avisado com anteced&ecirc;ncia caso haja necessidade de suspens&atilde;o de servi&ccedil;os.</p>
                    <p>Os atendimentos poder&atilde;o ser feitos por computadores de mesa, notebooks, tablets ou smartphones. S&atilde;o necess&aacute;rios c&acirc;mera e fones de ouvido, que s&atilde;o de responsabilidade total do psic&oacute;logo e usu&aacute;rio, para a realiza&ccedil;&atilde;o dos atendimentos por videoconfer&ecirc;ncia orientamos que sejam feitos testes para verificar o bom funcionamento dos aparelhos antes do in&iacute;cio da sess&atilde;o.</p>
                    <p>Acesso ao sistema: O login para acesso a plataforma ser&aacute; o e-mail cadastrado e a senha definida, que s&atilde;o de uso pessoal e intransfer&iacute;vel.</p>
                    <p><strong>Links Importantes</strong></p>
                    <p>Informa&ccedil;&otilde;es sobre a regula&ccedil;&atilde;o do atendimento podem ser encontradas nos sites do Conselho Federal de Psicologia (http://site.cfp.org.br/)</p>
                    <p>Link: C&oacute;digo de &eacute;tica Profissional do Psic&oacute;logo</p>
                    <p>Link: Resolu&ccedil;&atilde;o CFP n&deg; 11/2018</p>
                    <p><strong>TERMOS E CONDI&Ccedil;&Otilde;ES DE USO USU&Aacute;RIOS/CLIENTES/PACIENTES</strong></p>
                    <p>Ao usar ou acessar os Servi&ccedil;os da Plataforma O Psic&oacute;logo Online voc&ecirc; concorda publicamente e de forma legal com todo o conte&uacute;do expresso nesta Declara&ccedil;&atilde;o.</p>
                    <p>A Plataforma O Psic&oacute;logo Online permite conectar psic&oacute;logos e pacientes por meio de uma plataforma tecnol&oacute;gica online onde quer que eles estejam e em qualquer hor&aacute;rio, com &eacute;tica profissional, qualidade e seguran&ccedil;a.</p>
                    <p>A Plataforma O Psic&oacute;logo Online se compromete a oferecer e prestar os seguintes servi&ccedil;os aos clientes interessados atrav&eacute;s dos psic&oacute;logos cadastrados.</p>
                    <p>Servi&ccedil;o de atendimento online por chat, &aacute;udio ou videoconfer&ecirc;ncia atrav&eacute;s de ferramentas seguras adequadas para tal finalidade.</p>
                    <p>Acesso a todos os psic&oacute;logos cadastrados no banco de dados do site, apresentando a lista pr&eacute;via dos mesmo, que pode ser alterada a qualquer momento por op&ccedil;&atilde;o do site ou pela solicita&ccedil;&atilde;o de desligamento do pr&oacute;prio profissional.</p>
                    <p>Intermediar o pagamento dos servi&ccedil;os prestados pelo psic&oacute;logo cadastrado no site por meio de ferramentas seguras e adequadas para tal finalidade.</p>
                    <p><strong>Agendamento da sess&atilde;o</strong></p>
                    <p>O psic&oacute;logo receber&aacute; um email atrav&eacute;s do sistema onde o mesmo dever&aacute; responder em at&eacute; 24h e confirmar ou n&atilde;o a sess&atilde;o com o cliente. A sess&atilde;o s&oacute; ser&aacute; confirmada mediante o contato do psic&oacute;logo com o cliente.</p>
                    <p><strong>Mudan&ccedil;a no hor&aacute;rio da sess&atilde;o</strong></p>
                    <p>Caso o cliente deseje alterar o hor&aacute;rio da sess&atilde;o, dever&aacute; fazer isso pelo menos 24 horas antes do in&iacute;cio da sess&atilde;o marcada. O cliente deve cancelar a sess&atilde;o que havia agendado com 24 horas de anteced&ecirc;ncia e em seguida agendar nova sess&atilde;o nos dias e hor&aacute;rios desejados. N&atilde;o ser&aacute; poss&iacute;vel remarcar a sess&atilde;o se isso n&atilde;o for feito com pelo menos 24h de anteced&ecirc;ncia e o cliente perder&aacute; o direito de reagendar a sess&atilde;o.</p>
                    <p><strong>Cancelamento do valor pago pela sess&atilde;o</strong></p>
                    <p>O valor de cada sess&atilde;o &eacute; pago antecipadamente pelo cliente. Caso o cliente desista do atendimento, ele deve fazer o cancelamento da sess&atilde;o e solicitar a devolu&ccedil;&atilde;o dos valores junto ao psic&oacute;logo com pelo menos 24 horas de anteced&ecirc;ncia caso contr&aacute;rio o valor n&atilde;o ser&aacute; devolvido. Ser&atilde;o descontados do valor devolvido as taxas de transa&ccedil;&atilde;o. Caso queira a devolu&ccedil;&atilde;o dos valores pagos, siga os procedimentos acima e envie um email para o psic&oacute;logo solicitando a devolu&ccedil;&atilde;o dos valores.</p>
                    <p>Aten&ccedil;&atilde;o: Caso ocorra problemas t&eacute;cnicos ou de conex&atilde;o por parte dos profissionais cadastrados no site opsicologoonline.com.br a consulta ser&aacute; reagendada em nova data e hor&aacute;rio sem nenhum &ocirc;nus para o usu&aacute;rio.</p>
                    <p><strong>N&atilde;o comparecimento do cliente</strong></p>
                    <p>O psic&oacute;logo escolhido pelo cliente disponibiliza o dia e a hora para o atendimento do mesmo e fica aguardando os 50 minutos da sess&atilde;o. Caso o cliente n&atilde;o compare&ccedil;a nos dias e hor&aacute;rios marcados, independente dos motivos, o valor da sess&atilde;o n&atilde;o ser&aacute; devolvido. Caso n&atilde;o possa comparecer o cliente deve cancelar ou reagendar a sess&atilde;o coma te 24h de anteced&ecirc;ncia.</p>
                    <p><strong>N&atilde;o comparecimento do psic&oacute;logo</strong></p>
                    <p>Caso o psic&oacute;logo n&atilde;o comparece no dia e hora agendados para a sess&atilde;o, e o mesmo n&atilde;o avisar previamente o n&atilde;o comparecimento o cliente poder&aacute; reagendar a sess&atilde;o. Neste caso pedimos para o cliente entrar em contato por email para averiguarmos o n&atilde;o comparecimento do psic&oacute;logo.</p>
                    <p>Login e senha</p>
                    <p>A correta utiliza&ccedil;&atilde;o de login e senha &eacute; de responsabilidade do usu&aacute;rio e as mesmas s&atilde;o de uso pessoal e intransfer&iacute;vel!</p>
                    <p>Seguran&ccedil;a das informa&ccedil;&otilde;es</p>
                    <p>O Psic&oacute;logo deve manter o sigilo das informa&ccedil;&otilde;es de acordo com o que preza o C&oacute;digo de &Eacute;tica do Psic&oacute;logo vigente no Brasil. Todas as informa&ccedil;&otilde;es trocadas no atendimento psicol&oacute;gico online n&atilde;o s&atilde;o registradas. N&atilde;o h&aacute; nenhum tipo de registro das informa&ccedil;&otilde;es trocadas entre cliente e psic&oacute;logo.</p>
                    <p>Apesar da Plataforma O Psic&oacute;logo Online garantir o sigilo de todas as informa&ccedil;&otilde;es transitadas dentro do site, h&aacute; v&aacute;rios riscos envolvidos quando h&aacute; trocas de informa&ccedil;&otilde;es por meio da internet. O psic&oacute;logo segue o c&oacute;digo de &eacute;tica profissional e protege as informa&ccedil;&otilde;es do cliente. Orientamos aos usu&aacute;rios dos servi&ccedil;os de atendimento psicol&oacute;gico online a n&atilde;o usar computadores p&uacute;blicos, apagar o hist&oacute;rico das conversas e usar um bom ANTIV&Iacute;RUS.</p>
                    <p>Acesso a internet</p>
                    <p>A Plataforma O Psic&oacute;logo Online n&atilde;o &eacute; respons&aacute;vel pelo acesso a internet do usu&aacute;rio, sendo o acesso de total responsabilidade do mesmo. A Plataforma O Psic&oacute;logo Online se exime de qualquer responsabilidade quanto &agrave; indisponibilidade gerada por problemas t&eacute;cnicos do computador do usu&aacute;rio ou de conex&atilde;o do usu&aacute;rio e o impedimento de acesso a rede.</p>
                    <p>Para uma boa experi&ecirc;ncia de atendimento, &eacute; importante que o usu&aacute;rio possua uma boa conex&atilde;o com a internet, para que a sess&atilde;o com v&iacute;deo chat seja realizada sem problemas.</p>
                    <p>A Plataforma O Psic&oacute;logo Online visando a melhoria na qualidade dos servi&ccedil;os prestados, se reserva no direito de efetuar eventuais manuten&ccedil;&otilde;es em seu sistema. O usu&aacute;rio ser&aacute; avisado com anteced&ecirc;ncia caso haja necessidade de suspens&atilde;o de servi&ccedil;os.</p>
                    <p>S&atilde;o necess&aacute;rios c&acirc;mera e fones de ouvido, que s&atilde;o de responsabilidade total do usu&aacute;rio, para a realiza&ccedil;&atilde;o dos atendimentos por chat, &aacute;udio ou videoconfer&ecirc;ncia. Orientamos que o usu&aacute;rio fa&ccedil;a testes para verificar o bom funcionamento de seus aparelhos antes do in&iacute;cio da sess&atilde;o.</p>
                    <p><strong>Direitos autorais e de imagem</strong></p>
                    <p>Aten&ccedil;&atilde;o! &Eacute; estritamente proibido a grava&ccedil;&atilde;o de qualquer atendimento ou sess&atilde;o, independente de serem feitas por videoconfer&ecirc;ncia, &aacute;udio ou chat. S&atilde;o proibidas quaisquer, grava&ccedil;&otilde;es onde apare&ccedil;am imagens, sons, textos, ou outra linguagem criptografada ou n&atilde;o. Al&eacute;m das grava&ccedil;&otilde;es citadas anteriormente &eacute; estritamente proibido publicar, vincular ou mostrar a terceiros quaisquer registros em &aacute;udio, imagem, texto ou v&iacute;deo ou grava&ccedil;&otilde;es das sess&otilde;es, acarretando em crime de uso indevido da imagem e voz do psic&oacute;logo. Quando divulgar textos, est&aacute; incorrendo em crime de viola&ccedil;&atilde;o de direitos autorais.</p>
                    <p><strong>Links Importantes</strong></p>
                    <p>Informa&ccedil;&otilde;es sobre a regula&ccedil;&atilde;o do atendimento podem ser encontradas nos sites do Conselho Federal de Psicologia (http://site.cfp.org.br/)</p>
                    <p>Link: C&oacute;digo de &eacute;tica Profissional do Psic&oacute;logo</p>
                    <p>Link: Resolu&ccedil;&atilde;o CFP n&deg; 11/2018</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection