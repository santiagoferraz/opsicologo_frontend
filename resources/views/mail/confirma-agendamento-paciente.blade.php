
<table border="0" cellpadding="5" cellspacing="0" width="700" background="#f9f9f9">
    <tbody>
    <tr style="background:#f9f9f9;">
        <td>
            <font size="6" style="line-height:90px; text-align:center; font-family: 'arial', sans-serif;" color="#666666">Olá {{$dadosAgendamento['nomeCliente']}},</font>
            <br>

            <font size="3" style="font-family: 'arial', sans-serif;">
                <p>Você está recebendo este email por que agendou uma sessão de atendimento psicológico online na plataforma O Psicólogo Online para o dia <b>{{$dadosAgendamento['dataAgendamento']}} às {{$dadosAgendamento['horaAgendamento']}} horas (Horário de Brasília)</b>. Estamos aguardando a confirmação do pagamento para confirmarmos sua consulta!   </p>

                <br><center>

                    <b>Conclua o pagamento clicando abaixo:</b>
                    <br>
                    {!! $dadosAgendamento['botaoPagamento'] !!}

                <br>

                @if ($dadosAgendamento['hasDadosBancarios'])
                <br>
                <b>Ou com depósito/transferência bancária: </b>
                <br>
                {{$dadosAgendamento['dadosBancario']}}
                <br>
                <b>CPF: </b>{{$dadosAgendamento['cpf']}}
                <br><b>Obs.: </b>Em caso de deposito/transferência, favor enviar o comprovante para o psicólogo(a) em um dos contatos abaixo.<br>
                @endif
                </center>

                <p>P.S. Parabéns pela iniciativa em buscar ajuda profissional, O Psicólogo Online dispõe dos profissionais mais capacitados do mercado para melhor te atender, de forma ética e profissional!</p>

                <br>
                Atenciosamente,<br>
                {{$dadosAgendamento['nomePsicologo']}}<br>
                O Psicólogo Online<br><br>

                <br><center><font color="red">
                        Caso o botão de pagamento não funcione, acesse: <a href="https://atendimento.opsicologoonline.com.br/sistema/login.php"><b>https://atendimento.opsicologoonline.com.br/sistema/login.php</b></a>
                        <br>faça o <strong>LOGIN</strong>

                        @if ($dadosAgendamento['isUsuarioNovo']))
                        <br><br>
                        <strong>e-mail:</strong>  {{$dadosAgendamento['emailCliente']}} <br>
                        <strong>senha: </strong> {{$dadosAgendamento['senhaPaciente']}} <br><br>
                        @endif

                        e entre em "Consultas Marcadas" para ver as formas de pagamentos.
                    </font></center>
                <br>
            </font>
        </td>
    </tr>

    <tr>
        <td style="background:#f9f9f9;">
            <hr>
            <font size="2" style="font-family: 'arial', sans-serif; text-align:center;">
                Email enviado automáticamente pelo site www.opsicologoonline.com.br favor não responder,<br> em caso de dúvidas entre em contato conosco através da página de contato no site.<br>
                Para garantir o recebimento dos nossos emails,<br> favor adicione <b>contato@opsicologoonline.com.br</b> na sua lista de contatos.<br>

            </font>
        </td>
    </tr>
    </tbody>
</table>


