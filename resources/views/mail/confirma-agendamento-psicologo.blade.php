
<table border="0" cellpadding="5" cellspacing="0" width="700" background="#f9f9f9">
    <tbody>
    <tr style="background:#f9f9f9;">
        <td>
            <font size="6" style="line-height:90px; text-align:center; font-family: 'arial', sans-serif;" color="#666666">Olá Psicologo,</font>
            <br>

            <font size="3" style="font-family: 'arial', sans-serif;">
                <p>Foi agendado uma sessão de atendimento psicológico online na plataforma <b>O Psicólogo Online</b>.<br><br><b>Paciente:</b> {{$dadosAgendamento['nomeCliente']}}<br><b>Dia:</b> {{$dadosAgendamento['dataAgendamento']}}<br><b>Horário:</b> {{$dadosAgendamento['horaAgendamento']}} (Horário de Brasília)<br><br>Fique atento sobre o status de pagamento do paciente para confirmar o dia e horário da sessão agendado.</p>

                <p>Para maiores informações sobre a consulta agendada, acesse o sistema <b>O Psicologo Online</b> - <a href=https://atendimento.opsicologoonline.com.br/sistema/login.php><b>https://atendimento.opsicologoonline.com.br/sistema/login.php</b></a></p>

                <br>
                Atenciosamente,<br>
                O Psicólogo Online<br><br>

            </font>
        </td>
    </tr>

    <tr>
        <td style="background:#f9f9f9;">
            <hr>
            <font size="2" style="font-family: 'arial', sans-serif; text-align:center;">
                Email enviado automáticamente pelo site www.psi.opsicologoonline.com.br favor não responder,<br> em caso de dúvidas entre em contato conosco através da página de contato no site.<br>
                Para garantir o recebimento dos nossos emails,<br> favor adicione <b>sistema@opsicologoonline.com.br</b> na sua lista de contatos.<br>

            </font>
        </td>
    </tr>
    </tbody>
</table>