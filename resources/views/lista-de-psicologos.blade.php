@extends('estrutura-principal')

@section('css')
    <style>
        .fixed-top {
            background: linear-gradient(to right, #3c96ff 0%, #2dfbff 100%) !important;
        }
        select.form-control:not([size]):not([multiple]) {
            height: unset !important;
        }
        .form-control {
            padding: 15px 15px !important;
        }
        .btn-pesquisar {
            width: 100% !important;
            top: 50% !important;
            transform: translateY(-50%) !important;
            -webkit-transform: translateY(-50%) !important;
            -ms-transform: translateY(-50%) !important;
            transform: translateY(-57%) !important;
        }
        .div-filtro {
            text-align: center !important;
        }
        #filtro-psicologos {
            margin-top: 10px !important;
        }
    </style>
@endsection

@section('menu-principal')
    @include('menu-secundario-cabecalho')
@endsection

@section('content')
    <section id="blog" class="section">
        <div class="container">

            <div class="row" id="filtro-psicologos">
                <div class="div-filtro col-lg-4 col-md-8 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Nome</label>
                        <input type="text" class="form-control" id="nomePsicologo" name="name" placeholder="Nome do Psicólogo">
                    </div>
                </div>
                <div class="div-filtro col-lg-2 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label for="uf">UF</label>
                        <select class="form-control" id="uf">
                            <option value="T">Todos</option>
                        </select>
                        <div id="carregando-estados" style="text-align: center">
                            <img style="height: 60px" src="{{ asset('assets/img/carregando-medio.gif') }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="div-filtro col-lg-3 col-md-8 col-sm-8 col-xs-12">
                    <div class="form-group">
                        <label for="cidade">Cidade</label>
                        <select class="form-control" id="cidade">
                            <option value="T">Todos</option>
                        </select>
                        <div id="carregando-cidades" style="text-align: center">
                            <img style="height: 60px" src="{{ asset('assets/img/carregando-medio.gif') }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="div-filtro col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <button class="btn btn-common btn-effect btn-pesquisar" id="pesquisarPsicologos">Pesquisar</button>
                </div>
            </div>

            <div class="row" id="lista-psicologos"></div>
            <div id="carregando-psicologos" style="text-align: center">
                <img src="{{ asset('assets/img/carregando-medio.gif') }}" alt="">
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('assets/js/lista-psicologos.js') }}"></script>
@endsection




