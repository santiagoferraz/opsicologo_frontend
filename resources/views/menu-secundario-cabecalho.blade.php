<li class="nav-item">
    <a class="nav-link page-scroll" href="{{ URL::to('/') }}">Início</a>
</li>
<li class="nav-item">
    <a class="nav-link page-scroll" href="{{ URL::to('/') }}#app-features">Vantagens</a>
</li>
<li class="nav-item">
    <a class="nav-link page-scroll" href="{{ URL::to('/') }}#blog">Ver Psicólogos</a>
</li>
<li class="nav-item">
    <a class="nav-link page-scroll" href="https://pages.opsicologoonline.com.br/plataforma-opsicologoonline" target="_blank">PSI Trabalhe Conosco</a>
</li>
<li class="nav-item">
    <a class="nav-link page-scroll" href="{{ URL::to('/') }}#contact">Contato</a>
</li>
<li class="nav-item btn-cadastro-login">
    <a href="https://atendimento.opsicologoonline.com.br/sistema/cadastro.php" class="btn btn-lg btn-border btn-login">Cadastro</a>
</li>
<li class="nav-item btn-cadastro-login">
    <a href="https://atendimento.opsicologoonline.com.br/sistema/login.php" class="btn btn-lg btn-border btn-login">Login</a>
</li>