$(function () {

    $('#finalizacao-pagamento').hide();
    $("#div-texto-pagamento").hide();
    $("#div-texto-pagamento-2").hide();
    $('#horarios-disponiveis').hide();
    $("#carregando-finalizar").hide();

    var form = $("#dadosusuario");
    form.validate({
        rules: {
            nome: "required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            nome: "Por favor, insira um nome",
            email: "Por favor, insira um e-mail válido"
        }
    });



    $("#wizard").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex){

            if (currentIndex === 0) {
                if (!$.contains(document.body, form)) {
                    form.appendTo("#dadosuser");
                    form.css('display', 'inline');
                }
                var horario = $("input[name='horario']:checked").val();
                if(horario){
                    return true;
                } else {
                    $('#conteudoSelecionarHorario').html("<strong>Por favor, selecione um dia/horário!</strong>");
                    $('#modalSelecionarHorario').modal();
                }
                return false;
            }

            if (currentIndex === 1) {
                if (newIndex === 2) {
                    var texto = "Olá, " + $("#nome-user").val()  +"!<br>Por favor, confirme os dados abaixo antes de finalizar o agendamento:<br><br>" +
                        "Data<br><b>"+$('#datetimepicker12').data().date+"</b><br><br>"+
                        "Horário<br><b>"+ $("input[name='horario']:checked").val() +"</b><br><br>"+
                        "Email<br><b id='email-confirmacao'>"+$('#email-user').val()+"</b>";
                    $("#resumo-confirmacao").html(texto);
                    return form.valid();
                }
            }
            return true;
        },
        onFinished: function (event, currentIndex)
        {
            $("#resumo-confirmacao").hide();
            $("#carregando-finalizar").show();
            var dados = {
                emailPsicologo: $("#email-psicologo").val(),
                dataAgendamento: $('#datetimepicker12').data().date,
                horaAgendamento: $("input[name='horario']:checked").val(),
                emailCliente: $('#email-user').val(),
                nomeCliente: $("#nome-user").val()
            };

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "psicologo/finalizar-agendamento",
                data: {dados: dados},
                success: function(data){

                    if (data.success) {
                        $("#carregando-finalizar").hide();
                        $('#div-wizard').hide();
                        var texto = "<b>Sua consulta foi agendada com sucesso!</b><br><br>Enviamos todas as informações sobre a consulta para o e-mail <b>"+data.emailPaciente+"</b><br>Efetue o pagamento através do botão abaixo ou acesse seu e-mail para mais informações.<br><br>";
                        if (data.hasDadosBancarios) {
                            texto = "<b>Sua consulta foi agendada com sucesso!</b><br><br>Enviamos todas as informações sobre a consulta para o e-mail <b>"+data.emailPaciente+"</b><br>Efetue o pagamento através do botão abaixo, depósito/transferência bancária ou acesse seu e-mail para mais informações.<br>";
                            texto +="<br>"+data.dadosBancario+ "<br>" + "<b>CPF: </b>" + data.cpf + "<br>";
                            texto +="<br><b>Obs.: </b>Ao efetuar depósito/transferência, favor enviar o comprovante para o(a) psicólogo(a).<br><br>";
                        }

                        $("#div-texto-pagamento").append(texto);
                        $("#div-texto-pagamento").append(data.botaoPagamento);
                        $("#div-texto-pagamento").show();
                        $("#finalizacao-pagamento").show();
                    } else {
                        $('#div-wizard').hide();
                        $("#div-texto-pagamento-2").show();
                        $("#finalizacao-pagamento").show();

                    }

                }
            });


        }
    });
});

$(function () {


    var data = moment().add(1, 'days').format('YYYY-MM-DD');
    var idPsicologo = window.location.pathname.split('/');
    idPsicologo = idPsicologo[idPsicologo.length - 1];
    $.get("../psicologo/get-horarios-disponiveis/"+data+"/"+idPsicologo, function (data) {
        $('#horarios-disponiveis').empty();
        $('#horarios-disponiveis').show();
        $('#carregando-horarios').hide();
        if (data.length > 0) {
            for(var i = 0; i < data.length; i++) {
                $('#horarios-disponiveis').append(
                    '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">\n' +
                    '    <div class="radio-horario">\n' +
                    '        <input type="radio" name="horario" id="radioi'+i+'" class="form-radio" value="'+data[i].horario_disponivel.substring(0, 5)+'">\n' +
                    '        <label for="radioi'+i+'">'+data[i].horario_disponivel.substring(0, 5)+'</label>\n' +
                    '    </div>\n' +
                    '</div>'
                );
            }
        } else {
            $('#horarios-disponiveis').append(
                '<div class="div-hora-nao-disponivel col-lg-12 col-md-12 col-sm-12 col-xs-12">\n' +
                '    <div style="color: white">Não existem horários disponíveis para a data selecionada. Escolha outra data ou entre em contato com o(a) psicólogo(a) para combinar o melhor dia e horário para você.' +
                '    </div>' +
                '</div>'
            );
        }

    });

});

$(function () {

    $('#datetimepicker12').datetimepicker({
        inline: true,
        sideBySide: true,
        format: 'DD/MM/YYYY',
        locale: 'pt-br',
        minDate: moment().add(1, 'days').toDate(),
        defaultDate: moment().add(1, 'days').toDate()
    }).on("dp.change", function (e) {
        $('#carregando-horarios').show();
        $('#horarios-disponiveis').empty();
        var data = moment(e.date._d).format('YYYY-MM-DD');
        var idPsicologo = e.delegateTarget.baseURI.split('/');
        idPsicologo = idPsicologo[idPsicologo.length - 1];
        $.get("../psicologo/get-horarios-disponiveis/"+data+"/"+idPsicologo, function (data) {
            $('#horarios-disponiveis').show();
            $('#carregando-horarios').hide();
            if (data.length > 0) {
                for(let i = 0; i < data.length; i++) {
                    $('#horarios-disponiveis').append(
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">\n' +
                        '    <div class="radio-horario">\n' +
                        '        <input type="radio" name="horario" id="radioi'+i+'" class="form-radio" value="'+data[i].horario_disponivel.substring(0, 5)+'">\n' +
                        '        <label for="radioi'+i+'">'+data[i].horario_disponivel.substring(0, 5)+'</label>\n' +
                        '    </div>\n' +
                        '</div>'
                    );
                }
            } else {
                $('#horarios-disponiveis').append(
                    '<div class="div-hora-nao-disponivel col-lg-12 col-md-12 col-sm-12 col-xs-12">\n' +
                    '    <div style="color: white">Não existem horários disponíveis para a data selecionada. Escolha outra data ou entre em contato com o(a) psicólogo(a) para combinar o melhor dia e horário para você.' +
                    '    </div>' +
                    '</div>'
                );
            }


        });
    });
});
