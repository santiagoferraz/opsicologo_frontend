$(function () {

    var divPsicologosDestaque = $('#grid-list-psicologos-destaque');

    var formInscricao = $('#formInscricao');

    formInscricao.validate({
        rules: {
            emailInscricao: {
                required: true,
                email: true
            }
        },
        messages: {
            emailInscricao: "Por favor, insira um email válido"
        }
    });

    formInscricao.keypress(function(e) {
        if (e.which === 13) // Enter key = keycode 13
        {
        return false;
        }
    });

    $('#carregando-inscricao').hide();

    $('#inscricaoLeadLovers').click(function () {

        if (formInscricao.valid()) {

            var dados = {email : $('#emailInscricao').val()};

            $('#carregando-inscricao').show();
            $('#inscricaoLeadLovers').hide();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {dados: dados},
                dataType: 'html',
                type: "POST",
                url: "psicologo/inscricao-lead-lovers",
                success: function(data){
                    $('#carregando-inscricao').hide();
                    $('#inscricaoLeadLovers').show();
                    formInscricao.trigger("reset");
                    var texto = "Muito obrigado por se cadastrar!<br><br>\n" +
                        "\n" +
                        "Siga estes simples passos listados abaixo:<br>\n" +
                        "\n" +
                        "<strong>1 - Verifique seu email</strong><br>\n" +
                        "\n" +
                        "Confira seu email agora e encontre o email que acabamos de enviar. <br>\n" +
                        "\n" +
                        "<strong>2 - Procure pelo email enviado por Carlos Costa</strong> <br>\n" +
                        "\n" +
                        "Veja o email enviado com o título \"O Psicólogo Online\". <br>\n" +
                        "\n" +
                        "<strong>3 - Clique no Link de Confirmação</strong> <br>\n" +
                        "\n" +
                        "Confirme que nós temos a permissão de te enviar emails clicando no link dentro do email. <br>\n" +
                        "\n" +
                        "<strong>4 - Sua Recompensa Está a Caminho</strong> <br>\n" +
                        "\n" +
                        "Pronto, a partir de agora você irá receber emails com artigos e dicas exclusivas da plataforma O Psicólogo Online.";
                    $("#modalLeadLovers").html(texto);
                    $("#modalLeadLovers").modal();
                }
            });
        }

    });

    $.get('../psicologo/destaques', function (data) {
        $('#carregando-destaques').hide();
        var psicologos = data.psicologos;

        psicologos.forEach(function (psicologo) {
            var htmlPsicologo = "<div class=\"col-lg-4 col-md-6 col-xs-12 blog-item\">\n" +
                "                    <div class=\"blog-item-wrapper\">\n" +
                "                        <div class=\"spacing-img-list-perfil blog-item-img\">\n" +
                "                            <div class=\"div-img-list-psicologos\"><a href='"+psicologo.link+"'>\n" +
                "                                <img class=\"img-list-perfil\" src='"+psicologo.fotoPerfil+"' alt=\"\">\n" +
                "                            </a></div>\n" +
                "                            <h6>"+psicologo.nome+"</h6>\n" +
                "                            <span>CRP - "+psicologo.crp+"</span><br>\n" +
                "                            <div id=\"valor\"><b>R$ </b>"+psicologo.valor_consulta+"\n" +
                "                                <img src='assets/template-principal/img/icon-time.jpg'> 50 min\n" +
                "                            </div>\n" +
                "                        </div>\n" +
                "                        <div class=\"spacing-text-list-perfil blog-item-text\">\n" +
                "                            <div class=\"spacing-descricao-list-perfil\"><h3>"+psicologo.descricao+"</h3></div>\n" +
                "                            <div class=\"author\">\n" +
                "                                <span class=\"name\">Avaliações</span>\n" +
                "                                <span class=\"date float-right\">\n" +
                "                                    <i class=\"fa fa-star star-1\"></i>\n" +
                "                                    <i class=\"fa fa-star star-1 fill-100\"></i>\n" +
                "                                    <i class=\"fa fa-star star-1 fill-100\"></i>\n" +
                "                                    <i class=\"fa fa-star star-1 fill-100\"></i>\n" +
                "                                    <i class=\"fa fa-star star-1 fill-100\"></i>\n" +
                "                                </span>\n" +
                "\n" +
                "                            </div>\n" +
                "                            <a class='btn-mais-detalhes btn btn-common btn-effect' href='"+psicologo.link+"'>Mais detalhes\n" +
                "                            </a>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>";

            divPsicologosDestaque.append(htmlPsicologo);
        });
    });

    $('#enviando-fale-conosco').hide();
    var formFaleConosco = $("#contactForm");
    formFaleConosco.validate({
        rules: {
            nomeRemetente: "required",
            emailRemetente: {
                required: true,
                email: true
            },
            mensagemFaleConosco: "required"
        },
        messages: {
            nomeRemetente: "Por favor, insira um nome",
            emailRemetente: "Por favor, insira um email válido",
            mensagemFaleConosco: "Por favor, insira uma mensagem"
        }
    });

    $("#enviarFaleConosco").click(function (event) {
        if (formFaleConosco.valid()) {
            $('#enviando-fale-conosco').show();
            $('#enviarFaleConosco').hide();
            var dados = {
                emailRemetente: $("#emailRemetente").val(),
                nomeRemetente: $('#nomeRemetente').val(),
                assunto: $('#assunto').val(),
                mensagem: $('#mensagem').val()
            };

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "psicologo/fale-conosco",
                data: {dados: dados},
                success: function(data){
                    $('#enviando-fale-conosco').hide();
                    $('#enviarFaleConosco').show();
                    if (data.isEmailEnviado) {
                        formFaleConosco.trigger("reset");
                        $("#modalFaleConosco").html("<p>Olá, obrigado por entrar em contato conosco.\n" +
                            "        Seu email foi enviado com sucesso!\n" +
                            "        </p>");
                        $("#modalFaleConosco").modal();
                    } else {
                        $("#modalFaleConosco").html("<p>Ops! Desculpa, aconteceu um erro ao tentar enviar seu email.\n" +
                            "        Por favor, tente novamente mais tarde!\n" +
                            "        </p>");
                        $("#modalFaleConosco").modal();
                    }

                }
            });
        }
    });


});
