$(function () {

    $('#carregando-cidades').hide();
    $('#uf').hide();

    $.get('../psicologo/ufs', function (ufs) {
        $('#carregando-estados').hide();
        $('#uf').show();
        ufs.forEach(function (uf) {
           var option = "<option value='"+uf.id+"'>"+uf.uf+"</option>";
            $('#uf').append(option);
        });
    });

    $('#uf').change(function () {
        $('#cidade').hide();
        $('#cidade').empty();
        $('#carregando-cidades').show();
        var option = "<option value='T'>Todos</option>";
        $('#cidade').append(option);
        var id = $('#uf').val();
        if (id !== "T") {
            $.get('../psicologo/cidades/'+id, function (cidades) {
                $('#carregando-cidades').hide();
                $('#cidade').show();

                cidades.forEach(function (cidade) {
                    var option = "<option value='"+cidade.id+"'>"+cidade.nome+"</option>";
                    $('#cidade').append(option);
                });
            });
        } else {
            $('#carregando-cidades').hide();
            $('#cidade').show();
        }

    });

    var ultimaPesquisa = {
        nomePsicologo: null,
        uf: null,
        cidade: null
    };

    var divListaPsicologos = $('#lista-psicologos');
    var pagina = 0;
    var paginasCarregadas = [];
    var quantidadePaginas = 1;

    $('#pesquisarPsicologos').click(function () {
        pagina = 0;
        paginasCarregadas = [];
        ultimaPesquisa = {
            nomePsicologo: $('#nomePsicologo').val() !== "" ? $('#nomePsicologo').val() : null,
            uf: $('#uf').val() !== "T" ? $('#uf').val(): null,
            cidade: $('#cidade').val() !== "T" ? $('#cidade').val() : null
        };

        divListaPsicologos.empty();
        carregarPsicologos(true);
    });



    $('#carregando-psicologos').hide();
    carregarPsicologos(false);

    function carregarPsicologos(isPesquisa) {

        if (pagina < quantidadePaginas ) {
            $('#carregando-psicologos').show();
            var url = "get-lista-de-psicologos/" + pagina;
            if (!paginasCarregadas.includes(pagina)) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: url,
                    data: {dadosFiltro: ultimaPesquisa},
                    success: function(data){

                        quantidadePaginas = data.qtdPaginas;
                        var psicologos = data.psicologos;
                        $('#carregando-psicologos').hide();

                        if (psicologos && psicologos.length > 0) {

                            psicologos.forEach(function (psicologo) {
                                var htmlPsicologo = "<div class=\"col-lg-4 col-md-6 col-xs-12 blog-item\">\n" +
                                    "                    <div class=\"blog-item-wrapper\">\n" +
                                    "                        <div class=\"spacing-img-list-perfil blog-item-img\">\n" +
                                    "                            <div class=\"div-img-list-psicologos\"><a href='"+psicologo.link+"'>\n" +
                                    "                                <img class=\"img-list-perfil\" src='"+psicologo.fotoPerfil+"' alt=\"\">\n" +
                                    "                            </a></div>\n" +
                                    "                            <h6>"+psicologo.nome+"</h6>\n" +
                                    "                            <span>CRP - "+psicologo.crp+"</span>\n" +
                                    "                            <div id=\"valor\"><b>R$ </b>"+psicologo.valor_consulta+"\n" +
                                    "                                <img src='../assets/template-principal/img/icon-time.jpg'> 50 min\n" +
                                    "                            </div>\n" +
                                    "                        </div>\n" +
                                    "                        <div class=\"spacing-text-list-perfil blog-item-text\">\n" +
                                    "                            <div class=\"spacing-descricao-list-perfil\"><h3>"+psicologo.descricao+"</h3></div>\n" +
                                    "                            <div class=\"author\">\n" +
                                    "                                <span class=\"name\">Avaliações</span>\n" +
                                    "                                <span class=\"date float-right\">\n" +
                                    "                                    <i class=\"fa fa-star star-1\"></i>\n" +
                                    "                                    <i class=\"fa fa-star star-1 fill-100\"></i>\n" +
                                    "                                    <i class=\"fa fa-star star-1 fill-100\"></i>\n" +
                                    "                                    <i class=\"fa fa-star star-1 fill-100\"></i>\n" +
                                    "                                    <i class=\"fa fa-star star-1 fill-100\"></i>\n" +
                                    "                                </span>\n" +
                                    "\n" +
                                    "                            </div>\n" +
                                    "                            <a class='btn-mais-detalhes btn btn-common btn-effect' href='"+psicologo.link+"'>Mais detalhes\n" +
                                    "                            </a>\n" +
                                    "                        </div>\n" +
                                    "                    </div>\n" +
                                    "                </div>";

                                divListaPsicologos.append(htmlPsicologo);
                            });
                        } else {
                            divListaPsicologos.empty();
                            divListaPsicologos.append("Não foi encontrado nenhum psicólogo para o filtro aplicado.");
                        }

                        pagina++;

                    }
                });
                paginasCarregadas.push(pagina);
            }
        }



    }

    $(window).scroll(function () {

        var total = $(document).height() - $(window).height();
        var atual = $(window).scrollTop();
        var valorCalculado = (total*55)/100;
        if (atual >= valorCalculado) {
            carregarPsicologos(false);
        }
    });



});
