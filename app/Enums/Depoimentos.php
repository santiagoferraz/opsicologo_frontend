<?php
/**
 * Created by PhpStorm.
 * User: santiago.ferraz@hotmail.com
 * Date: 17/05/2017
 * Time: 16:20
 */

namespace App\Enums;


abstract class Depoimentos
{
    const URL_IMG = 'assets/template-principal/img/testimonial/';

    const DEPOIMENTOS = [
        [
            'img' => 'img-f-1.jpg',
            'nome' => 'Maria A S Moura',
            'texto' => 'Iniciei na plataforma e fiz meu primeiro atendimento online o qual foi uma experiência 
                        fantástica. Minha paciênte estava em Nova York, me achou na plataforma, e desde o agendamento
                        até o atendimento, ocorreu perfeitamente bem. Obrigado por ter proporcionado a estrutura e as 
                        condições para que isso ocorresse e deixasse tanto o profissional quanto o paciênte em um sentimento 
                        de segurança.',
            'tipo' => 'Psicóloga'
        ],
        [
            'img' => 'img-m-1.jpg',
            'nome' => 'Sandro Almeida',
            'texto' => 'Simplesmente incrível! Com "O Psicólogo Online" eu consegui elevar minha auto-estima 
                        e abrir minha mente para novas formas de ver o mundo. Melhorou definitivamente minhas relações 
                        familiares e profissionais.',
            'tipo' => 'Paciênte'
        ],
        [
            'img' => 'img-f-2.jpg',
            'nome' => 'Daniele Oliveira',
            'texto' => 'Recentemente me procuraram do Jornal Folha de Londrina para uma reportagem sobre terapia online. 
                        O pessoal me achou através do "O Psicologo Online". Estou muito feliz com o acontecido 
                        e super satisfeita com a plataforma.',
            'tipo' => 'Psicóloga'
        ],
        [
            'img' => 'img-m-2.jpg',
            'nome' => 'Pedro Henrique',
            'texto' => 'Procurei ajuda para conseguir me libertar da timidez e com isso melhorar meu desempenho 
                        profissional. Estou adorando as sessões, minha psicóloga é ótima e já posso dizer que 
                        estou conseguindo obter resultados. Recomendo muito a plataforma!',
            'tipo' => 'Paciênte'
        ]
    ];
}