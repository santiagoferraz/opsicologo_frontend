<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmaAgendamentoPsicologo extends Mailable
{
    use Queueable, SerializesModels;

    protected $dados;
    protected $psicologo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dados, $psicologo)
    {
        $this->dados = $dados;
        $this->psicologo = $psicologo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.confirma-agendamento-psicologo', ['dadosAgendamento'=> $this->dados]);
    }
}
