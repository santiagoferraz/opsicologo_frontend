<?php
/**
 * Created by PhpStorm.
 * User: Luis Fernando
 * Date: 07/03/2017
 * Time: 11:44
 */

namespace App\Console\Commands;

use App\Http\Controllers\PsicologosController;
use Illuminate\Console\Command;

class FilaPsicologo extends Command
{
    protected
        $signature = 'FilaPsicologo:run',
        $description = 'Altera a ordem dos psicologos.';

    public function handle()
    {
        $psicologoController = new PsicologosController();
        $psicologoController->alterarOrdemPsicologos();
    }

}