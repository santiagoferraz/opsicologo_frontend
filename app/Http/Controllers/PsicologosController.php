<?php

namespace App\Http\Controllers;

use App\Enums;
use App\Model\Cidade;
use App\Model\Estado;
use App\Model\Fila;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class PsicologosController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

    }

    public function montarSqlFiltro ($sql, $dadosFiltro) {
        if (isset($dadosFiltro['nomePsicologo'])) {
            $sql = $sql->where('nome', 'LIKE', '%'.$dadosFiltro['nomePsicologo'].'%');
        }

        if (isset($dadosFiltro['uf'])) {
            $sql = $sql->where('estado', '=', $dadosFiltro['uf']);
        }

        if (isset($dadosFiltro['cidade'])) {
            $sql = $sql->where('cidade', '=', $dadosFiltro['cidade']);
        }

        return $sql;
    }

    public function getListaPsicologos(Request $request, $pagina) {
        $dadosFiltro = $request->get('dadosFiltro');
        $itensPorPagina = 9;
        $itens = $pagina * $itensPorPagina;
        $sql = $this->getSelectPsicologos();

        $sql = $this->montarSqlFiltro($sql, $dadosFiltro);

        $qtd = round($sql->count() / $itensPorPagina) ;
        $quantidadePaginas = $qtd == 0 ? 1 : $qtd ;
        $orderByAndLimit = " ORDER BY posicao ASC LIMIT ".$itensPorPagina." OFFSET ".$itens;

        $sql = $this->getSql($sql).$orderByAndLimit;

        $psicologos = DB::table(DB::raw("({$sql}) r"))->get();

        foreach ($psicologos as $psicologo) {
            $psicologo->fotoPerfil = isset($psicologo->codigo) ? Enums\Diversos::DIR_IMAGENS.$psicologo->codigo : '../assets/img/foto-perfil-default.png';
            $psicologo->descricao = mb_convert_encoding(strip_tags($psicologo->descricao), 'UTF-8', 'UTF-8');
            $psicologo->descricao = substr($psicologo->descricao, 0, 295) . "...";
            $psicologo->nome = mb_convert_encoding(substr($this->tratarNome($psicologo->nome), 0, 24), 'UTF-8', 'UTF-8');
            $psicologo->link = env('APP_URL') . $psicologo->link;
            $psicologo->valor_consulta = str_replace(".",",", money_format('%.2n', (int)$psicologo->valor_consulta));
        }

        return response()->json(array('psicologos' => $psicologos, 'qtdPaginas' => $quantidadePaginas));
    }


    public function getSql($query)
    {
        $sql = $query->toSql();
        foreach($query->getBindings() as $binding)
        {
            $value = is_numeric($binding) ? $binding : "'".$binding."'";
            $sql = preg_replace('/\?/', $value, $sql, 1);
        }
        return $sql;
    }

    public function getSelectPsicologos () {
        $sql = "select 
                (select valor from valores_consultas where id_psicologo = usuarios.id order by valor LIMIT 1) as valor_consulta,
                id, nome, descricao, crp, link, codigo, posicao, cidade, estado from usuarios 
                left join fila on usuarios.id = fila.id_psicologo
                where usuarios.status = 'Ativo' 
                and usuarios.nivel = 'Psicologo' 
                and usuarios.crp is not null 
                and usuarios.crp <> ''  
                and usuarios.link is not null  
                and usuarios.link <> '' ";


        return DB::table(DB::raw("({$sql}) x"))->whereRaw("valor_consulta is not null and valor_consulta <> '' and valor_consulta > 0");
    }

    public function getPsicologosDestaque () {
        $psicologosDestaque = $this->getSelectPsicologos()
                                   ->orderBy('posicao', 'asc')
                                   ->limit(6)
                                   ->get();


        foreach ($psicologosDestaque as $psicologo) {
            $psicologo->fotoPerfil = isset($psicologo->codigo) ? Enums\Diversos::DIR_IMAGENS.$psicologo->codigo : '../assets/img/foto-perfil-default.png';
            $psicologo->descricao = mb_convert_encoding(strip_tags($psicologo->descricao), 'UTF-8', 'UTF-8');
            $psicologo->descricao = substr($psicologo->descricao, 0, 295) . "...";
            $psicologo->nome = mb_convert_encoding(substr($this->tratarNome($psicologo->nome), 0, 24), 'UTF-8', 'UTF-8');
            $psicologo->link = env('APP_URL') . $psicologo->link;
            $psicologo->valor_consulta = str_replace(".",",", money_format('%.2n', (int)$psicologo->valor_consulta));
        }
        return response()->json(array('psicologos' => $psicologosDestaque));

    }

    public function getListaPsicologosView()
    {
        return view('lista-de-psicologos');
    }


    public function tratarNome($nome)
    {
        $nome = strtolower($nome); // Converter o nome todo para minúsculo
        $nome = explode(" ", $nome); // Separa o nome por espaços
        $saida = "";
        for ($i=0; $i < count($nome); $i++) {

            // Tratar cada palavra do nome
            if ($nome[$i] == "de" or $nome[$i] == "da" or $nome[$i] == "e" or $nome[$i] == "dos" or $nome[$i] == "do") {
                $saida .= $nome[$i].' '; // Se a palavra estiver dentro das complementares mostrar toda em minúsculo
            }else {
                $saida .= ucfirst($nome[$i]).' '; // Se for um nome, mostrar a primeira letra maiúscula
            }

        }
        return $saida;
    }

    public function getUfs () {
        return Estado::get();
    }

    public function getCidades ($idEstado) {
        return Cidade::where('estado', '=', (int)$idEstado)->get();
    }

    public function alterarOrdemPsicologos () {
        $this->addPsicologoNaFila();
        $this->removePsicologoInaptosNaFila();

        $fila = Fila::select()->orderBy('posicao', 'asc')->get();

        DB::beginTransaction();
        try {
            foreach ($fila as $key => $f) {
                $f->posicao = $key+1;
                $f->save();
                DB::commit();
            }
        } catch (\Exception $e) {
            DB::rollBack();
        }
        $this->reordenarFila();
    }

    public function reordenarFila() {
        DB::beginTransaction();
        try {
            $fila = (new Fila())->orderBy('posicao', 'desc')->get();
            foreach ($fila as $key => $f) {
                if ($f->posicao == 1) {
                    $f->posicao = (new Fila())->count();
                } else {
                    $f->posicao = $f->posicao - 1;
                }

                $f->save();
                DB::commit();
            }
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    public function addPsicologoNaFila() {
        DB::beginTransaction();
        try {
            $psicologosNaoAtivosFila = $this->getSelectPsicologos()
                                            ->select('x.id')
                                            ->where('x.posicao', '=', null)
                                            ->get();

            $posicao = Fila::select('posicao')->orderBy('posicao', 'desc')->limit(1)->get()->toArray()[0]['posicao'];

            foreach ($psicologosNaoAtivosFila as $psicologo) {
                $fila = new Fila();
                $fila->id_psicologo = $psicologo->id;
                $fila->posicao = $posicao + 1;
                $fila->save();
                $posicao++;
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    public function removePsicologoInaptosNaFila() {
        DB::beginTransaction();
        try {
            $psicologosAptos = $this->getSelectPsicologos()
                                    ->select('id')
                                    ->get();

            $psicologosAptos = $psicologosAptos->map(function ($item) { return $item->id; });

            $psicologosInaptos = (new Fila())->whereNotIn('id_psicologo', $psicologosAptos)->get();

            foreach ($psicologosInaptos as $inapto) {
                $fila = (new Fila())->where('id_psicologo', $inapto->id_psicologo)->delete();
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }
}


