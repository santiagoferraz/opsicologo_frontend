<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmaAgendamentoPaciente;
use App\Mail\ConfirmaAgendamentoPsicologo;
use App\Model\ConsultasAgendadas;
use App\Model\HorariosDisponiveis;
use App\Model\Usuarios;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ConsultasController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function getConsultas()
    {
        return view('consultas', compact('psicologo'));
    }

    public function getHorariosDisponiveis($data, $linkPsicologo)
    {

        $psicologo = (new Usuarios())->select('id')->where('link', $linkPsicologo)->get()->first();
        $horariosRetorno = $this->getHorarios($data, $psicologo->id);

        return $horariosRetorno;
    }

    public function criarAgendamento (Request $request) {
        $dadosAgendamento = $request->get('dados');
        $usuario = Usuarios::where('email', '=', $dadosAgendamento['emailCliente'])->first();
        $psicologo = Usuarios::where('email', '=', $dadosAgendamento['emailPsicologo'])->first();
        $date_array = explode("/",$dadosAgendamento['dataAgendamento']);
        $dataConsulta = "$date_array[0]-$date_array[1]-$date_array[2]";
        if (isset($usuario)) {
            $dadosAgendamento['isUsuarioNovo'] = false;
            $consulta = new ConsultasAgendadas();
            $consulta->data_consulta = new Carbon($dataConsulta);
            $consulta->hora_consulta = $dadosAgendamento['horaAgendamento'];
            $consulta->id_paciente = $usuario->id;
            $consulta->id_psicologo = $psicologo->id;
            $consulta->status = 'Pendente';
            $consulta->tipo_consulta = 1;
            $consulta->save();
        } else {
            $dadosAgendamento['isUsuarioNovo'] = true;
            $usuario = new Usuarios();
            $usuario->nome = $dadosAgendamento['nomeCliente'];
            $usuario->email = $dadosAgendamento['emailCliente'];
            $usuario->nivel = 'Paciente';
            $usuario->telefone = '';
            $usuario->status = 'Ativo';
            $senha = (new UtilController())->geraSenha(8, true, true, false);
            $dadosAgendamento['senhaPaciente'] = $senha;
            $usuario->senha = md5($senha);
            $dataCadastro = new Carbon();
            $dataCadastro->setTimezone(new \DateTimeZone('America/Sao_Paulo'));
            $usuario->data_cadastro = $dataCadastro;
            $horaCadastro = new Carbon();
            $horaCadastro->setTimezone(new \DateTimeZone('America/Sao_Paulo'));
            $usuario->hora_cadastro = $horaCadastro->toTimeString();
            $usuario->skype = '';
            $usuario->save();

            $consulta = new ConsultasAgendadas();
            $consulta->data_consulta = new Carbon($dataConsulta);
            $consulta->hora_consulta = $dadosAgendamento['horaAgendamento'];
            $consulta->id_paciente = $usuario->id;
            $consulta->id_psicologo = $psicologo->id;
            $consulta->status = 'Pendente';
            $consulta->tipo_consulta = 1;
            $consulta->save();
        }
        $botaoPagamento = "<center>".$psicologo->botao_pgto."</center>";
        $retorno = array('botaoPagamento' => $botaoPagamento, 'success' => true, 'isEmailEnviado' => false, 'emailPaciente' => $dadosAgendamento['emailCliente']);
        $dadosAgendamento['nomePsicologo'] = $psicologo->nome;
        $dadosAgendamento['botaoPagamento'] = $botaoPagamento;
        $dadosAgendamento['dadosBancario'] = isset($psicologo->conta) ? $psicologo->conta : '';
        $dadosAgendamento['cpf'] = $psicologo->cpf;
        $dadosAgendamento['hasDadosBancarios'] = isset($psicologo->conta) && $psicologo->conta != '' && isset($psicologo->cpf) && $psicologo->cpf != '';
        $retorno['dadosBancario'] = $dadosAgendamento['dadosBancario'];
        $retorno['cpf'] = $dadosAgendamento['cpf'];
        $retorno['hasDadosBancarios'] = $dadosAgendamento['hasDadosBancarios'];
        Mail::to($dadosAgendamento['emailCliente'], $dadosAgendamento['nomeCliente'])->send(new ConfirmaAgendamentoPaciente($dadosAgendamento));
        Mail::to($psicologo->email, $psicologo->nome)->send(new ConfirmaAgendamentoPsicologo($dadosAgendamento, $psicologo));
        Mail::to(env('EMAIL_COPIA_AGENDAMENTO'), $psicologo->nome)->send(new ConfirmaAgendamentoPsicologo($dadosAgendamento, $psicologo));

        if (!Mail::failures()) {
            $retorno['isEmailEnviado'] = true;
        }
        return response()->json($retorno);
    }

    public function getHorarios($data, $idPsicologo)
    {
        $sql = "
            SELECT DISTINCT
                (CASE WHEN (SELECT ca.id FROM consultas_agendadas ca
                            WHERE ca.id_psicologo = hd.id_psicologo
                            AND ca.data_consulta = hd.dia
                            AND (
                                  ( ca.hora_consulta BETWEEN hd.horario_inicial AND (SELECT ADDTIME(hd.horario_inicial, '00:50:00')))
                                  OR    ((SELECT ADDTIME(ca.hora_consulta, '00:50:00')) BETWEEN hd.horario_inicial AND (SELECT ADDTIME(hd.horario_inicial, '00:50:00')))
                                  OR    (hd.horario_inicial BETWEEN ca.hora_consulta AND (SELECT ADDTIME(ca.hora_consulta, '00:50:00')))
                                  OR    ((SELECT ADDTIME(hd.horario_inicial, '00:50:00')) BETWEEN ca.hora_consulta AND (SELECT ADDTIME(ca.hora_consulta, '00:50:00')))
                                )
                ) IS NULL
                THEN hd.horario_inicial
                END) as horario_disponivel
            FROM horarios_disponiveis hd
            WHERE
                hd.id_psicologo = ".$idPsicologo."
                AND hd.dia = '".$data."'
        ";

        return DB::table(DB::raw("({$sql}) sub"))
            ->where('horario_disponivel', '!=', null)
            ->orderBy('horario_disponivel', 'asc')
            ->get();
    }
}
