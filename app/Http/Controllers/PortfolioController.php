<?php

namespace App\Http\Controllers;

use App\Model\Usuarios;
use App\Model\ValoresConsultas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use \App\Enums;

class PortfolioController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

    }

    public function index()
    {
        return view('portfolio');
    }

}
