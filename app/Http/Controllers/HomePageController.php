<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmaAgendamentoPaciente;
use App\Mail\FaleConosco;
use App\Model\Usuarios;
use App\Model\ValoresConsultas;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use \App\Enums;

class HomePageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

    }

    public function index($linkPsicologo)
    {
        $psicologo = (new Usuarios())->where('link', $linkPsicologo)->get()->first();

        if(!isset($psicologo) || $psicologo->status != "Ativo") {
            return redirect('psicologo/lista-de-psicologos');
        }

        $psicologo->fotoPerfil = $psicologo->codigo ? Enums\Diversos::DIR_IMAGENS.$psicologo->codigo : 'assets/img/foto-perfil-default.png';
        $psicologo->consulta = (new ValoresConsultas())->where('id_psicologo', '=', $psicologo->id)->orderBy('valor', 'asc')->first();

        if (isset($psicologo->consulta )) {
            $psicologo->consulta->valor = str_replace(".",",", money_format('%.2n', (int)$psicologo->consulta->valor));
        } else {
            $psicologo->consulta = new \stdClass();
            $psicologo->consulta->valor = 0;
        }

        return view('homepage', compact('psicologo'));
    }

    public function valor()
    {
        $psicologo = (new Usuarios())->limit(500)->get();

        return $psicologo->toArray();
    }

    public function enviarFaleConosco (Request $request) {
        $dadosFaleConosco = $request->get('dados');

        if (!isset($dadosFaleConosco['assunto'])) {
            $dadosFaleConosco['assunto'] = '';
        }
         Mail::to(env('EMAIL_FALE_CONOSCO'), $dadosFaleConosco['nomeRemetente'])->send(new FaleConosco($dadosFaleConosco));
        $retorno = array('isEmailEnviado' => false);
        if (!Mail::failures()) {
            $retorno['isEmailEnviado'] = true;
        }
        return response()->json($retorno, 200);
    }

    public function inscricaoLeadLovers (Request $request) {
        $dadosInscricao = $request->get('dados');
        $client = new Client();

        $response = $client->request('POST', 'https://leadlovers.com/Pages/Index/54674', [
            'form_params' => [
                'email' => $dadosInscricao['email']
            ]
        ]);

        return $response->getBody()->getContents();

    }

}
