<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmaAgendamentoPaciente;
use App\Mail\FaleConosco;
use App\Model\Usuarios;
use App\Model\ValoresConsultas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use \App\Enums;

class PagesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

    }

    public function pagePoliticaPrivacidade () {
        return view('politica-de-privacidade');
    }

    public function pageQuemSomos () {
        return view('quem-somos');
    }

    public function pageTermoUso () {
        return view('termos-de-uso');
    }

    public function pagePerguntasFrequentes () {
        return view('perguntas-frequentes');
    }

}
