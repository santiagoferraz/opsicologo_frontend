<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ValoresConsultas extends Model
{
    protected $table = 'valores_consultas';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_psicologo',
        'tipo',
        'descricao',
        'valor',
    ];
    public $timestamps = false;

}
