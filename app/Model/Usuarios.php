<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'cidade',
        'codigo',
        'conta',
        'cpf',
        'crp',
        'data_cadastro',
        'descricao',
        'email',
        'estado',
        'hora_cadastro',
        'nascimento',
        'nivel',
        'nome',
        'senha',
        'skype',
        'status',
        'telefone',
    ];

    public $timestamps = false;

}
