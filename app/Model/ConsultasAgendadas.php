<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ConsultasAgendadas extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'data_consulta',
        'hora_consulta',
        'id_paciente',
        'id_psicologo',
        'status',
        'tipo_consulta',
    ];

    public $timestamps = false;
}
