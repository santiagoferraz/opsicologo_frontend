<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $table = 'cidade';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'nome',
        'estado'
    ];
    public $timestamps = false;
}
