<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Fila extends Model
{
    protected $table = 'fila';
    protected $primaryKey = 'id_psicologo';
    protected $fillable = [
        'id_psicologo',
        'posicao'
    ];
    public $timestamps = false;
}
