<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HorariosDisponiveis extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'dia',
        'horario_final',
        'horario_inicial',
        'id_psicologo',
    ];

    public $timestamps = false;
}
