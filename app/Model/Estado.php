<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'estado';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'nome',
        'uf',
        'pais'
    ];
    public $timestamps = false;
}
